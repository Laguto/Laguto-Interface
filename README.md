# Laguto

Laguto reads the telemetry output of the official Formula1 Game and provides processed ready-to-display data via web sockets.

[Website](https://laguto.de) - [Documentation](https://docs.laguto.de)

# Authors

- [Felix Grohme](https://social.lauercloud.de/@felix)
- [Mike Lauer](https://social.lauercloud.de/@mike)