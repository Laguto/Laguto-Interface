﻿using System.Net;
using System.Net.Sockets;
using Laguto.Lib.EventArgs;
using Laguto.Lib.Utility;

namespace Laguto.Lib.Listener;

public class UDPListener : IListener
{
    public int Port { get; private set; }
    private CancellationTokenSource _token;

    //This has to be in the IListener Interface
    public event EventHandler<UdpPacketReceivedEventArgs> UdpPacketReceivedEvent; 

    public UDPListener(int port)
    {
        Port = port;
        _token = new CancellationTokenSource();
    }

    public void StartListening()
    {
        Console.WriteLine($"Starting UDP Listening on {Port}");
        Task.Run(() =>
        {
            using var udpClient = new UdpClient(Port);
            while (!_token.IsCancellationRequested)
            {
                var anyIP = new IPEndPoint(IPAddress.Any, 0);
                var package = udpClient.Receive(ref anyIP);
                UdpPacketReceivedEvent.Invoke(null, new UdpPacketReceivedEventArgs(package));
            }
        });
    }
    
    
}