﻿namespace Laguto.Lib.Listener;

public interface IListener
{
    public void StartListening();
}