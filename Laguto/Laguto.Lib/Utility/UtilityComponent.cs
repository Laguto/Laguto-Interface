﻿using System.Diagnostics;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using Laguto.Lib.Models;

namespace Laguto.Lib.Utility;

public class UtilityComponent
{
    public List<int> PositionToDriverIndex { get; set; }
    private List<Color> PositionToColor { get; set; }
    public static int PlayerIndex { get; private set; }
    
    private static MD5 _md5 = MD5.Create();


    public UtilityComponent()
    {
        PositionToDriverIndex = new List<int>();
        PositionToColor = new List<Color>();    
        PlayerIndex = -1;
        for(var i = 0; i < 22; i++)
        {
            PositionToDriverIndex.Add(0);
            PositionToColor.Add(Color.Black);
        }
    }

    public void UpdateLap(Lap lap)
    {
        try
        {
            for (var i = 0; i < 22; i++)
            {
                SetDriverIndexAtPosition(i, lap.FieldLapData[i].CarPosition);
            }
        } catch(Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateParticipants(Participants participants)
    {
        try
        {
            for (var i = 0; i < 22; i++)
            {
                PositionToColor[GetPositionFromDriverIndex(i) - 1] =
                    CreateColorFromName(participants.FieldParticipantData[i].Name);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateSession(Session session)
    {
        if (session.IsSpectating)
            PlayerIndex = session.SpectatedCarIndex;
        else
            PlayerIndex = session.Header.PlayerCarIndex;
    }
    
    public int GetDriverIndexFromPosition(int position)
    {
        // If position = 0, empty driver slot, return index of last driver, namely 21
        if (position <= 0)
            return 21;
        return PositionToDriverIndex[position - 1];
    }

    public int GetPositionFromDriverIndex(int index)
    {
        if(PositionToDriverIndex.Contains(index))
            return PositionToDriverIndex.IndexOf(index) + 1;

        return 22;
    }
    public void SetDriverIndexAtPosition(int index, int position) {
        if (position <= 0)
            return;
        PositionToDriverIndex[position - 1] = index;
    }

    public Color GetColorFromPosition(int position)
    {
        if (position <= 0)
            return Color.White;
        return PositionToColor[position - 1];
    }

    //This is called "magic"
    public static Color CreateColorFromName(string name)
    {
        var hash = _md5.ComputeHash(Encoding.UTF8.GetBytes(name));
        return Color.FromArgb(hash[0], hash[1], hash[2]) ;
    }
}