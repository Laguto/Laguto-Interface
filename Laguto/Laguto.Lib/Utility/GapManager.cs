﻿using Laguto.Lib.Components.Models;

namespace Laguto.Lib.Utility;

public class GapManager
{
    const float MeterPerMeasurePoint = 50;
    // am überlegen
    private Dictionary<int, Dictionary<int, GapModel>> _measurePointDriverTimestampList;

    public GapManager()
    {
        _measurePointDriverTimestampList = new Dictionary<int, Dictionary<int, GapModel>>();
    }

    public long SetAndGetTimestampFromDistance(int driverIndex, float lapDistance, int lapNumber)
    {
        Dictionary<int, GapModel> timestampsAtMeasurePoint = GetTimestampsFromDistance(lapDistance);


        if(timestampsAtMeasurePoint.ContainsKey(driverIndex) && timestampsAtMeasurePoint[driverIndex].LapNumber == lapNumber)
            // Timestamp has already been set in current lap
            return timestampsAtMeasurePoint[driverIndex].Timestamp;


        long timestampSelf = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
        if(timestampsAtMeasurePoint.ContainsKey(driverIndex))
        {
            // Update timestamp
            GapModel gapModel = timestampsAtMeasurePoint[driverIndex];
            gapModel.LapNumber = lapNumber;
            gapModel.Timestamp = timestampSelf;
        } else
        {
            // Create new timestamp
            GapModel gapModel = new GapModel();
            gapModel.LapNumber = lapNumber;
            gapModel.Timestamp = timestampSelf;
            timestampsAtMeasurePoint.Add(driverIndex, gapModel);
        }
        return timestampSelf;
       
    }

    public Dictionary<int, GapModel> GetTimestampsFromDistance(float lapDistance)
    {
        int measurePointIndex = (int)(lapDistance / MeterPerMeasurePoint);

        if(!_measurePointDriverTimestampList.ContainsKey(measurePointIndex)) 
            _measurePointDriverTimestampList.Add(measurePointIndex, new Dictionary<int, GapModel>());

        Dictionary<int, GapModel> timestampsAtMeasurePoint = _measurePointDriverTimestampList[measurePointIndex];

        if (timestampsAtMeasurePoint == null)
        {
            timestampsAtMeasurePoint = new Dictionary<int, GapModel>();
            _measurePointDriverTimestampList[measurePointIndex] = timestampsAtMeasurePoint;
        }

        return timestampsAtMeasurePoint;
    }
        
        
}