﻿using Laguto.Lib.Enums;
using Laguto.Lib.EventArgs;
using Laguto.Lib.Listener;
using Laguto.Lib.Models;
using Laguto.Lib.PackageFactories;

namespace Laguto.Lib.Handlers;

public class PackageHandler
{
    private UDPListener _listener;
    public event EventHandler<CarDamagePackageEventArgs> CarDamagePackageReceivedEvent; 
    public event EventHandler<CarStatusPackageEventArgs> CarStatusPackageReceivedEvent; 
    public event EventHandler<FinalClassificationPackageEventArgs> FinalClassificationPackageReceivedEvent;
    public event EventHandler<LapPackageEventArgs> LapPackageReceivedEvent;
    public event EventHandler<ParticipantPackageEventArgs> ParticipantPackageReceivedEvent;
    public event EventHandler<SessionHistoryEventArgs> SessionHistoryPackageReceivedEvent;
    public event EventHandler<SessionPackageEventArgs> SessionPackageReceivedEvent;
    public event EventHandler<CarTelemetryPackageEventArgs> CarTelemetryPackageReceivedEvent;
    public event EventHandler<EventPackageEventArgs> EventPackageReceivedEvent; 
    
    #region unused events
    public event EventHandler<CarSetupPackageEventArgs> CarSetupPackageReceivedEvent;
    public event EventHandler<MotionPackageEventArgs> MotionPackageReceivedEvent;
    public event EventHandler<LobbyPackageEventArgs> LobbyPackageReceivedEvent; 
    
    #endregion         

     //If event is extracted to interface change this to "IListener listener"
    public PackageHandler(UDPListener listener)
    {
        _listener = listener;
        listener.UdpPacketReceivedEvent += ListenerOnUdpPacketReceivedEvent;
        _listener.StartListening();
    }
    
    private void ListenerOnUdpPacketReceivedEvent(object? sender, UdpPacketReceivedEventArgs e)
    {
        Header packageHeader = new HeaderPackageFactory().CreateFromBytes(e.Buffer);

        switch (packageHeader.PacketType)
        {
            case PacketType.CarDamage:
                var carDamage = new CarDamagePackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                carDamage.Header = packageHeader;
                CarDamagePackageReceivedEvent?.Invoke(null, new CarDamagePackageEventArgs(carDamage));
                break;
            case PacketType.FinalClassification:
                var finalClassification = new FinalClassificationPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                finalClassification.Header = packageHeader;
                FinalClassificationPackageReceivedEvent?.Invoke(null, new FinalClassificationPackageEventArgs(finalClassification));
                break;
            case PacketType.Session:
                var session = new SessionPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                session.Header = packageHeader;
                SessionPackageReceivedEvent?.Invoke(null, new SessionPackageEventArgs(session));
                break;
            case PacketType.Lap:
                var lap = new LapPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                lap.Header = packageHeader;
                LapPackageReceivedEvent?.Invoke(null, new LapPackageEventArgs(lap));
                break;
            case PacketType.Participants:
                var participants = new ParticipantsPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                participants.Header = packageHeader;
                ParticipantPackageReceivedEvent?.Invoke(null, new ParticipantPackageEventArgs(participants));
                break;
            case PacketType.SessionHistory:
                var sessionHistory = new SessionHistoryPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                sessionHistory.Header = packageHeader;
                SessionHistoryPackageReceivedEvent?.Invoke(null, new SessionHistoryEventArgs(sessionHistory));
                break;
            case PacketType.CarStatus:
                var carStatus = new CarStatusPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                carStatus.Header = packageHeader;
                CarStatusPackageReceivedEvent?.Invoke(null, new CarStatusPackageEventArgs(carStatus));
                break;
            case PacketType.CarTelemetry:
                var carTelemetry = new CarTelemetryPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                carTelemetry.Header = packageHeader;
                CarTelemetryPackageReceivedEvent?.Invoke(null, new CarTelemetryPackageEventArgs(carTelemetry));
                break;
            case PacketType.Event:
                var eventEntry = new EventPackageFactory().CreateFromBytes(e.Buffer, packageHeader.PackageFormat);
                eventEntry.Header = packageHeader;
                EventPackageReceivedEvent?.Invoke(null, new EventPackageEventArgs(eventEntry));
                break;
            case PacketType.Unknown:
            case PacketType.Motion:
            case PacketType.CarSetup:
            case PacketType.LobbyInfo:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}