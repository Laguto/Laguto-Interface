﻿using Laguto.Lib.Components.Models;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;
using System.Diagnostics;

namespace Laguto.Lib.Components;

public class LapTimeComponent : AbstractComponent
{
    private readonly UtilityComponent _utilityComponent;
    public int PlayerPosition { get; set; }
    
    public LapTimeModel[] LapTimes
    {
        get => _lapTimes
            .Values
            .OrderBy(x => x.Position)
            .ToArray();
    }
    
    private Dictionary<int, LapTimeModel> _lapTimes;

    public LapTimeComponent(UtilityComponent utilityComponent)
    {
        _utilityComponent = utilityComponent;
        _lapTimes = new Dictionary<int, LapTimeModel>();
    }

    public void UpdateLap(Lap lap)
    {
        if (UtilityComponent.PlayerIndex == -1)
            return;
        try
        {
                PlayerPosition = lap.FieldLapData[UtilityComponent.PlayerIndex].CarPosition;
        } catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    
    public void UpdateSessionHistory(SessionHistory sessionHistory)
    {
        try { 
            var lapTimes = sessionHistory.LapHistoryData;
            var position = _utilityComponent.GetPositionFromDriverIndex(sessionHistory.CarIndex);

            uint[] tempLapTimes = new uint[sessionHistory.NumberOfLaps - 1];
            for (var i = 0; i < sessionHistory.NumberOfLaps - 1; i++)
            {
                tempLapTimes[i] = lapTimes[i].LapTimeInMs;
            }
            
            var lapTimeModel = new LapTimeModel
            {
                Position = position,
                Color = _utilityComponent.GetColorFromPosition(position),
                LapTimes = tempLapTimes
            };
        
            if (_lapTimes.ContainsKey(sessionHistory.CarIndex))
            {
                _lapTimes[sessionHistory.CarIndex] = lapTimeModel;
            }
            else
            {
                _lapTimes.Add(sessionHistory.CarIndex, lapTimeModel);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}