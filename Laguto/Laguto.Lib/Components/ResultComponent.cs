﻿using Laguto.Lib.Components.Models;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;
using Newtonsoft.Json;

namespace Laguto.Lib.Components;

public class ResultComponent : AbstractComponent
{
    private readonly UtilityComponent _utilityComponent;

    [JsonIgnore]
    public bool IsRaceFinished { get; set; }
    public FinalClassification FinalClassification { get; set; }

    private Dictionary<int, SessionHistory> _sessionHistories { get; set; }
    public SessionHistory[] SessionHistories
    {
        get {
            foreach (SessionHistory history in _sessionHistories.Values)
            {
                history.LapHistoryData = history.LapHistoryData.TakeWhile(lapHistory => lapHistory.LapTimeInMs > 0).ToArray();
                history.TyreStintHistoryData = history.TyreStintHistoryData.TakeWhile(tyreStint => tyreStint.EndLap > 0).ToArray();
            }
            return _sessionHistories.Values.ToArray();
        }
    }

    private List<DriverInformation> _driverInfo;
    public DriverInformation[] DriverInformation { get => _driverInfo.ToArray(); }

    public ResultComponent(UtilityComponent utilityComponent)
    {
        _utilityComponent = utilityComponent;
        _sessionHistories = new Dictionary<int, SessionHistory>();
        _driverInfo = new List<DriverInformation>();
        for(var i = 0; i < 22; i++)
        {
            _driverInfo.Add(new DriverInformation());
        }
        IsRaceFinished = false;
    }

    public void UpdateParticipants(Participants participants)
    {
        try { 
            for(var i = 0; i < 22; i++)
            {
                _driverInfo[i].Name = participants.FieldParticipantData[i].Name;
                _driverInfo[i].IsAiControlled = participants.FieldParticipantData[i].IsAiControlled;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateFinalClassification(FinalClassification finalClassification)
    {
        FinalClassification = finalClassification;
        IsRaceFinished = true;
    }

    public void UpdateSessionHistory(SessionHistory sessionHistory)
    {
        try { 
            if (_sessionHistories.ContainsKey(sessionHistory.CarIndex))
            {
                _sessionHistories[sessionHistory.CarIndex] = sessionHistory;
            }
            else
            {
                _sessionHistories.Add(sessionHistory.CarIndex, sessionHistory);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
}