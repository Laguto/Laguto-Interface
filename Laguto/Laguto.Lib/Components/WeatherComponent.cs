﻿using Laguto.Lib.Components.Models;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;

namespace Laguto.Lib.Components;

public class WeatherComponent : AbstractComponent
{
    private readonly UtilityComponent _utilityComponent;

    public WeatherModel[] WeatherModels
    {
        get => _weatherModels.ToArray();
    }
    
    private List<WeatherModel> _weatherModels;

    public WeatherComponent(UtilityComponent utilityComponent)
    {
        _utilityComponent = utilityComponent;
        _weatherModels = new List<WeatherModel>();
    }

    public void UpdateSession(Session session)
    {
        try { 
            _weatherModels = new List<WeatherModel>();
            WeatherForecastSample[] samples = session.WeatherForecastSamples;

            for (var i = 0; i < session.NumberOfWeatherForecastSamples; i++)
            {
                WeatherForecastSample currentSample = samples[i];

                if (currentSample.TimeOffSet % 5 != 0)
                    continue;

                _weatherModels.Add(new WeatherModel
                {
                    WeatherCondition = currentSample.ForecastedWeatherCondition,
                    AirTemperature = currentSample.AirTemperatureCelsius,
                    OffsetTime = currentSample.TimeOffSet,
                    RainPercentage = currentSample.RainPercentage,
                    SessionType = currentSample.SessionTypeMode,
                    TrackTemperature = currentSample.TrackTemperatureCelsius
                });
            }

            _weatherModels.Sort((x, y) => x.OffsetTime - y.OffsetTime);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    
}