﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Components.Models;

public class WeatherModel
{
    public int OffsetTime { get; set; }
    public WeatherCondition WeatherCondition { get; set; }
    public double RainPercentage { get; set; }
    
    public double TrackTemperature { get; set; }
    public double AirTemperature { get; set; }
    
    public SessionType SessionType { get; set; }
}