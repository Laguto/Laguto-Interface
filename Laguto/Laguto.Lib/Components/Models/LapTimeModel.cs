﻿using System.Drawing;

namespace Laguto.Lib.Components.Models;

public class LapTimeModel
{
    public int Position { get; set; }
    public Color Color { get; set; }
    public uint[] LapTimes { get; set; }
}