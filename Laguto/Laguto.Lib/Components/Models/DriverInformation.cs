﻿namespace Laguto.Lib.Components.Models
{
    public class DriverInformation
    {
        public string Name { get; set; }
        public bool IsAiControlled { get; set; }
    }
}
