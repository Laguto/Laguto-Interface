﻿using System.Drawing;
using Laguto.Lib.Components.Enums;
using Laguto.Lib.Enums;

namespace Laguto.Lib.Components.Models;

public class TableRowModel 
{
    public int currentSector { get; set; } // Sector, not index

    public int PlayerIndex { get; set; }
    // Generel information
    public Color TeamColor { get; set; }
    public int Position { get; set; }
    public string DriverName { get; set; }
    public bool IsAI { get; set; }
    public DriverResultStatus Status { get; set; }
    public int PenaltiesCombined { get; set; }    
    public int PositionChange { get; set; }

    // Lap times
    public uint BestLapTime { get; set; }
    public int DeltaToBestLapTime { get; set; }
    public int DeltaToBestLapTimeOfDriverInFront { get; set; }
    public uint LastLapTime { get; set; }
    public int DeltaLastLapToDriverInFront { get; set; }

    // Gap times
    public int GapInterval { get; set; }
    public bool LappedInterval { get; set; }
    public int GapLeader { get; set; }
    public bool LappedLeader { get; set; }
    
    // Tyre information
    public TyreCompound TyreCompound { get; set; }
    public int TyreAge { get; set; }
    public int PitCount { get; set; }
    
    // Best sector times
    public uint BestSector1Time { get; set; }
    public uint BestSector2Time { get; set; }
    public uint BestSector3Time { get; set; }
    
    // Last sector times
    public uint LastSector1Time { get; set; }
    public uint LastSector2Time { get; set; }
    public uint LastSector3Time { get; set; }
}