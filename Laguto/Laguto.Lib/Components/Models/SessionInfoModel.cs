﻿namespace Laguto.Lib.Components.Models;

public class SessionInfoModel
{
    public int NumberActiveCars { get; set; }
    public List<int> PositionToDriverIndex { get; set; }

    public SessionInfoModel(int numberActiveCars)
    {
        PositionToDriverIndex = new List<int>();
        NumberActiveCars = numberActiveCars;
        for (var i = 0; i < NumberActiveCars; i++)
        {
            PositionToDriverIndex.Add(-1);
        }
    }
    
    public int GetDriverIndexFromPosition(int position)
    {
        return PositionToDriverIndex[position - 1];
    }

    public int GetPositionFromDriverIndex(int index)
    {
        return PositionToDriverIndex.IndexOf(index);
    }
}