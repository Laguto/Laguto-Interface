﻿namespace Laguto.Lib.Components.Models
{
    public class CarDamageModel
    {
        public int FrontLeftWingDamage { get; set; }
        public int FrontRightWingDamage { get; set; }
        public int RearWingDamage { get; set; }
        public int FloorDamage { get; set; }
        public int DiffuserDamage { get; set; }
        public int SidepodDamage { get; set; }
    }
}
