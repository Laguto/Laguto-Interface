﻿using System.Drawing;

namespace Laguto.Lib.Components.Models
{
    public class LapPositionModel
    {
        public int LapDistance;
        public Color Color;
        public int Position;
        public string Name;
        public bool IsAiControlled;
    }
}
