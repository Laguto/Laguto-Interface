﻿namespace Laguto.Lib.Components.Models
{
    public class GapModel
    {
        public int LapNumber { get; set; }
        public long Timestamp { get; set; }
    }
}
