﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;

namespace Laguto.Lib.Components;

public class SessionComponent : AbstractComponent
{
    //Header
    public int SessionTime { get; set; }
    
    //Session
    public SessionType SessionType { get; set; }
    public Track SessionTrack { get; set; }
    public SessionLength SessionLength { get; set; }
    public int SessionTimeLeft { get; set; }
    public int AirTemperature { get; set; }
    public int TrackTemperature { get; set; }
    
    
    public SessionComponent()
    {
        
    }


    public void UpdateSession(Session session)
    {
        SessionTime = session.SessionDuration;
        SessionType= session.SessionTypeMode;
        SessionTrack = session.SessionTrack;
        SessionLength = session.SessionLength;
        SessionTimeLeft = session.SessionTimeLeft;
        AirTemperature = session.AirTemperatureCelsius;
        TrackTemperature = session.TrackTemperatureCelsius;
    }
    
}