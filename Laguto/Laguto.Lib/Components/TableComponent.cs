﻿using Laguto.Lib.Components.Enums;
using Laguto.Lib.Components.Models;
using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;
using System.Diagnostics;

namespace Laguto.Lib.Components;

public class TableComponent : AbstractComponent
{
    private readonly UtilityComponent _utilityComponent;
    private int _trackLength = int.MaxValue;

    public int PlayerIndex { get => UtilityComponent.PlayerIndex; }
    public TableRowModel[] RowModels
    {
        get => _rowModels.Values.ToArray();
    }
    private Dictionary<int, TableRowModel> _rowModels { get; set; }
    private GapManager _gapManager;

    public SessionType SessionType;
  
    
    public TableComponent(UtilityComponent utilityComponent)
    {
        _utilityComponent = utilityComponent;
        _gapManager = new GapManager();
        
        _rowModels = new Dictionary<int, TableRowModel>();
        for (int i = 0; i < 22; i++)
        {
            _rowModels[i] = new TableRowModel();
        }
    }
    public void UpdateParticipants(Participants participants)
    {
        try { 
            for (var i = 0; i < 22; i++)
            {
                _rowModels[i].PlayerIndex = i;
                
                UpdateParticipantValues(i, participants.FieldParticipantData[i]);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateSession(Session session)
    {
        this._trackLength = session.TrackLengthMeters;
        this.SessionType = session.SessionTypeMode;
    }
    
    private void UpdateParticipantValues(int rowIndex, Participants.ParticipantData data)
    {
        try { 
            TableRowModel row = _rowModels[rowIndex];

            row.DriverName = data.Name;
            row.IsAI = data.IsAiControlled;
        
            row.TeamColor = UtilityComponent.CreateColorFromName(data.Name);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateSessionHistory(SessionHistory sessionHistory)
    {
        try { 
            TableRowModel row = _rowModels[sessionHistory.CarIndex];

            var bestTimeLap = sessionHistory.BestLapTimeLapNumber;
            if(bestTimeLap > 0)
                row.BestLapTime = sessionHistory.LapHistoryData[bestTimeLap - 1].LapTimeInMs;

            row.DeltaToBestLapTime = GetDeltaToBestLapTime(sessionHistory.CarIndex, row.BestLapTime);

            row.DeltaToBestLapTimeOfDriverInFront = GetDeltaToBestLapTimeOfDriverInFront(sessionHistory.CarIndex, row.BestLapTime);

            var bestTimeSector1Lap = sessionHistory.BestSector1LapNumber;
            if(bestTimeSector1Lap > 0)
                row.BestSector1Time = sessionHistory.LapHistoryData[bestTimeSector1Lap - 1].Sector1TimeInMs;
        
            var bestTimeSector2Lap = sessionHistory.BestSector2LapNumber;
            if(bestTimeSector2Lap > 0)
                row.BestSector2Time = sessionHistory.LapHistoryData[bestTimeSector2Lap - 1].Sector2TimeInMs;
        
            var bestTimeSector3Lap = sessionHistory.BestSector3LapNumber;
            if(bestTimeSector3Lap > 0)
                row.BestSector3Time = sessionHistory.LapHistoryData[bestTimeSector3Lap - 1].Sector3TimeInMs;
         
            if (row.currentSector != 3 && sessionHistory.NumberOfLaps > 1)
                row.LastSector3Time = sessionHistory.LapHistoryData[sessionHistory.NumberOfLaps - 2].Sector3TimeInMs;
            else
                row.LastSector3Time = 0;
            }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public void UpdateLap(Lap lap)
    {
        try { 
            for (var i = 0; i < 22; i++)
            {
                if (!_rowModels.ContainsKey(i))
                {
                    _rowModels[i] = new TableRowModel();
                    continue;
                }
                
                UpdateLapValues(i, lap.FieldLapData[i], lap.FieldLapData);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    
    private void UpdateLapValues(int rowIndex, Lap.LapData data, Lap.LapData[] allDriverData)
    {
        TableRowModel row = _rowModels[rowIndex];

        row.currentSector = data.Sector + 1;

        row.Position = data.CarPosition;

        row.LastLapTime = data.LastLapTimeInMs;

        row.DeltaLastLapToDriverInFront = GetDeltaToLastLapTimeOfDriverInFront(rowIndex, data);

        row.Status = GetDriverStatus(data.CurrentDriverStatus, data.FinalResultStatus, data.CurrentPitStatus);
        
        row.LastSector1Time = data.Sector1TimeInMs;
        if(data.Sector == 2) // In sector 3
            row.LastSector2Time = data.Sector2TimeInMs;
        if (data.Sector == 1)
            row.LastSector2Time = 0;

        row.PitCount = data.NumberPitStops;

        row.PenaltiesCombined = data.Penalties;

        row.PositionChange = data.StartingGridPosition - row.Position;


        if(row.Position == 1)
        {
            // Leader is not lapped
            row.LappedInterval = false;
            row.LappedLeader = false;
            row.GapInterval = 0;
            row.GapLeader = 0;

            // Set timestamp for gap computation for following drivers
            _gapManager.SetAndGetTimestampFromDistance(rowIndex, data.TotalDistance, data.CurrentLapNumber);
        } else
        {
            /**
             * Lapped = (My-TotalDistance - Front-TtotalDistance) > TrackLength
             */ 

            // Gap/Lapped Interval
            int driverIndexInFront = _utilityComponent.GetDriverIndexFromPosition(row.Position - 1);
            float differenceToDriverInFront = allDriverData[driverIndexInFront].TotalDistance - data.TotalDistance;
            row.LappedInterval = differenceToDriverInFront > this._trackLength;
            if(row.LappedInterval == false)
                row.GapInterval = GetGapInterval(rowIndex, data);

            // Gap/Lapped Leader
            int leaderIndex = _utilityComponent.GetDriverIndexFromPosition(1);
            float differenceToLeader = allDriverData[leaderIndex].TotalDistance - data.TotalDistance;
            row.LappedLeader = differenceToLeader > this._trackLength;
            if(row.LappedLeader == false)
                row.GapLeader = GetGapLeader(rowIndex, data);
        }       
    }

    public void UpdateCarStatus(CarStatus carStatus)
    {
        try { 
            for (var i = 0; i < 22; i++)
            {
                if (!_rowModels.ContainsKey(i))
                {
                    _rowModels[i] = new TableRowModel();
                    continue;
                }

                UpdateCarStatusValues(i, carStatus.FieldCarStatusData[i]);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    private void UpdateCarStatusValues(int rowIndex, CarStatus.CarStatusData data)
    {
        TableRowModel row = _rowModels[rowIndex];

        row.TyreCompound = data.EquippedVisualTyreCompound;
        row.TyreAge = data.TyreAgeLaps;
    }

    private int GetGapInterval(int rowIndex, Lap.LapData data)
    {
        if (data.TotalDistance < 0)
            return 0;

        if (data.CarPosition == 1)
            return 0;
        
        long timestampSelf = _gapManager.SetAndGetTimestampFromDistance(rowIndex, data.TotalDistance, data.CurrentLapNumber);

        long timestampDriverInFront = _gapManager.SetAndGetTimestampFromDistance(_utilityComponent.GetDriverIndexFromPosition(data.CarPosition - 1), data.TotalDistance, data.CurrentLapNumber);

        return (int) (timestampSelf - timestampDriverInFront);
    }       

    private int GetGapLeader(int rowIndex, Lap.LapData data)
    {
        if (data.CarPosition == 1)
            return 0;

        long timestampSelf = _gapManager.SetAndGetTimestampFromDistance(rowIndex, data.TotalDistance, data.CurrentLapNumber);

        long timestampLeadingDriver = _gapManager.SetAndGetTimestampFromDistance(_utilityComponent.GetDriverIndexFromPosition(1), data.TotalDistance, data.CurrentLapNumber);

        return (int) (timestampSelf - timestampLeadingDriver);
    }
    
    private int GetDeltaToBestLapTime(int carIndex, uint selfBestLapTime)
    {
        if (selfBestLapTime == 0)
            return 0;

        uint bestLapTime = uint.MaxValue;

        foreach(TableRowModel row in _rowModels.Values)
        {
            if(row.BestLapTime > 0 && row.BestLapTime < bestLapTime)
            {
                bestLapTime = row.BestLapTime;
            }
        }

        return (int) (selfBestLapTime - bestLapTime);
    }

    private int GetDeltaToBestLapTimeOfDriverInFront(byte carIndex, uint bestLapTime)
    {
        if (bestLapTime == 0)
            return 0;

        int position = _utilityComponent.GetPositionFromDriverIndex(carIndex);

        if (position == 1)
            return 0;

        int driverInFrontIndex = _utilityComponent.GetDriverIndexFromPosition(position - 1);

        return (int) (bestLapTime - _rowModels[driverInFrontIndex].BestLapTime);
    }

    private int GetDeltaToLastLapTimeOfDriverInFront(int rowIndex, Lap.LapData data)
    {
        if (data.CarPosition == 1)
            return 0; // Driver is in first place

        int driverInFrontIndex = _utilityComponent.GetDriverIndexFromPosition(data.CarPosition - 1);
        TableRowModel driverInFront = _rowModels[driverInFrontIndex];

        return (int)(_rowModels[rowIndex].LastLapTime - driverInFront.LastLapTime);
    }

    private DriverResultStatus GetDriverStatus(DriverStatus driverStatus, ResultStatus resultStatus, PitStatus pitStatus)
    {
        switch(resultStatus)
        {
            case ResultStatus.DSQ:
                return DriverResultStatus.DSQ;
            case ResultStatus.DNF:
                return DriverResultStatus.DNF;
            case ResultStatus.Retired:
                return DriverResultStatus.Retired;
            case ResultStatus.Finished:
                return DriverResultStatus.Finished;
        }

        switch(driverStatus)
        {
            case DriverStatus.InGarage:
                return DriverResultStatus.InGarage;
        }

        switch(pitStatus)
        {
            case PitStatus.PitLane:
            case PitStatus.PitArea:
                return DriverResultStatus.InPit;
        }
        

        return (DriverResultStatus) driverStatus;
    }   
    
    
}