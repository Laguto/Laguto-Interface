﻿namespace Laguto.Lib.Components.Enums;

public enum DriverResultStatus
{
    InGarage ,
    FlyingLap ,
    InLap ,
    OutLap ,
    OnTrack ,
    Finished ,
    Retired,
    DNF,
    DSQ,
    InPit
}