﻿using Laguto.Lib.Components.Models;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;
using static Laguto.Lib.Models.CarDamage;

namespace Laguto.Lib.Components
{
    public class CarDamageComponent : AbstractComponent
    {
        private UtilityComponent _utilityComponent;

        public CarDamageModel DamageModel { get; private set; }

        public CarDamageComponent(UtilityComponent utilityComponent)
        {
            _utilityComponent = utilityComponent;
            DamageModel = new CarDamageModel();
        }

        public void UpdateCarDamage(CarDamage carDamage)
        {
            if (UtilityComponent.PlayerIndex == -1)
                return;

            try
            {               
                CarDamageData damageData = carDamage.FieldCarStatusData[UtilityComponent.PlayerIndex];
                DamageModel.FrontLeftWingDamage = damageData.FrontLeftWingDamage;
                DamageModel.FrontRightWingDamage = damageData.FrontRightWingDamage;
                DamageModel.RearWingDamage = damageData.RearWingDamage;
                DamageModel.FloorDamage = damageData.FloorDamage;
                DamageModel.SidepodDamage = damageData.SidepodDamage;
                DamageModel.DiffuserDamage = damageData.DiffuserDamage;
            } catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
