﻿using Laguto.Lib.Components.Models;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;
using System.Diagnostics;

namespace Laguto.Lib.Components
{
    public class LapPositionComponent : AbstractComponent
    {
        private readonly UtilityComponent _utilityComponent;
        public int TrackLength { get; private set; }

        private List<LapPositionModel> _trackPositions;
        public LapPositionModel[] TrackPositions
        {
            get => _trackPositions.ToArray();
        }

        public int NumberOfMarshallZones { get; set; }
        public MarshallZone[] MarshallZones { get; set; }

        public int PlayerIndex { get => UtilityComponent.PlayerIndex; }

        public LapPositionComponent(UtilityComponent utilityComponent)
        {
            _utilityComponent = utilityComponent;
            TrackLength = 0;
            _trackPositions = new List<LapPositionModel>();
            for(var i = 0; i < 22; i++)
            {
                _trackPositions.Add(new LapPositionModel());
            }
        }


        public void UpdateSession(Session session)
        {
            // trackLength
            TrackLength = session.TrackLengthMeters;

            NumberOfMarshallZones = session.NumberOfMarshallZones;
            MarshallZones = session.MarshallZones;
        }

        public void UpdateLap(Lap lap)
        {
            try
            {
                for (var i = 0; i < 22; i++)
                {
                    Lap.LapData data = lap.FieldLapData[i];
                    _trackPositions[i].LapDistance = (int)data.LapDistance;
                    _trackPositions[i].Position = data.CarPosition;
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void UpdateParticipants(Participants participants)
        { 
            try {
                for (var i = 0; i < 22; i++)
                {
                    Participants.ParticipantData data = participants.FieldParticipantData[i];
                    _trackPositions[i].Name = data.Name;
                    _trackPositions[i].Color = UtilityComponent.CreateColorFromName(data.Name);
                    _trackPositions[i].IsAiControlled = data.IsAiControlled;
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

}
