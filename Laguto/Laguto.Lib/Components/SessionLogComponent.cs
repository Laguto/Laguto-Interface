﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;

namespace Laguto.Lib.Components;

public class SessionLogComponent : AbstractComponent
{
    private List<Event> _sessionLog { get; set; }
    
    public Event[] SessionLog
    {
        get => _sessionLog.ToArray();
    }

    public SessionLogComponent()
    {
        _sessionLog = new List<Event>();
    }

    public void UpdateEvent(Event eventEntry)
    {
        if (eventEntry.EventType == EventType.Buttons || eventEntry.EventType == EventType.Flashback)
            return;
        _sessionLog.Add(eventEntry);
    }
    
    
}