﻿
namespace Laguto.Lib.Enums
{
    public enum ErsDeployMode
    {
        None = 0,
        Medium = 1,
        HotLap = 2,
        Overtake = 3
    }
}
