﻿namespace Laguto.Lib.Enums;

public enum PackageFormat
{
    F12021 = 2021,
    F12022 = 2022
}