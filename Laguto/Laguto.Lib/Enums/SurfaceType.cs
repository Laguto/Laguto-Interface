﻿
namespace Laguto.Lib.Enums
{
    public enum SurfaceType
    {
        Tarmac = 0,
        RumbleStrip = 1,
        Concrete = 2,
        Rock = 3,
        Gravel = 4,
        Mud = 5,
        Sand = 6
    }
}
