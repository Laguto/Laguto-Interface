﻿namespace Laguto.Lib.Enums;

public enum EventType
{
    SessionStarted,
    SessionEnded,
    FastestLap,
    Retirement,
    RcDRSEnabled,
    RcDRSDisabled,
    TeamMateInPits,
    CheckeredFlagWaved,
    RaceWinner,
    Penalty,
    SpeedTrap,
    StartLights,
    LightsOut,
    DriveThroughPenaltyServed,
    StopGoPenaltyServed,
    Flashback,
    Buttons,
    Unknown
}