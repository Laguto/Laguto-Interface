﻿
namespace Laguto.Lib.Enums
{
    public enum ResultStatus
    {
        Invalid,
        Inactive,
        Active,
        Finished,
        DNF,
        DSQ,
        NotClassified,
        Retired
    }
}
