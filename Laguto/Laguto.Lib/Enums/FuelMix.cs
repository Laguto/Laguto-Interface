﻿
namespace Laguto.Lib.Enums
{
    public enum FuelMix
    {
        Lean = 0,
        Standard = 1,
        Rich = 2,
        Max = 3
    }
}
