﻿
namespace Laguto.Lib.Enums
{
    public enum PitStatus
    {
        OnTrack,
        PitLane,
        PitArea
    }
}
