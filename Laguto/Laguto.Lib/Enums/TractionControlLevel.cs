﻿
namespace Laguto.Lib.Enums
{
    public enum TractionControlLevel
    {
        Off,
        Low,
        High
    }
}
