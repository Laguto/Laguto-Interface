﻿
namespace Laguto.Lib.Enums
{
    public enum FormulaType
    {
        Formula1Modern,
        Formula1Classic,
        Formula2,
        Formula1Generic
    }
}
