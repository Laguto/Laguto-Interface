﻿
namespace Laguto.Lib.Enums
{
    public enum DriverStatus
    {
        InGarage,
        FlyingLap,
        InLap,
        OutLap,
        OnTrack
    }
}
