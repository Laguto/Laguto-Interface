﻿
namespace Laguto.Lib.Enums
{
    public enum PacketType
    {
        Unknown = 999,
        Motion = 0,
        Session = 1, 
        Lap = 2,
        Event = 3,
        Participants = 4,
        CarSetup = 5, 
        CarTelemetry = 6,
        CarStatus = 7, 
        FinalClassification = 8, 
        LobbyInfo = 9, 
        CarDamage = 10,
        SessionHistory = 11
    }
}
