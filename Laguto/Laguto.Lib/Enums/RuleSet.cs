﻿
namespace Laguto.Lib.Enums
{
    public enum RuleSet
    {
        PracticeAndQualifying = 0,
        Race = 1,
        TimeTrial = 2,
        TimeAttack = 4,
        CheckpointChallenge = 6,
        Autocross = 8,
        Drift = 9,
        AverageSpeedZone = 10,
        RivalDuel = 11
    }
}
