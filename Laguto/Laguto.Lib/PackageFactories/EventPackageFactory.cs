﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class EventPackageFactory : AbstractFactory<Event>
{

    public override Event CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        Event returnEvent = new Event();

        var eventTypeChar = byteArrayManager.NextBytes(4);
        var eventTypeString = System.Text.Encoding.UTF8.GetString(eventTypeChar);

        switch (eventTypeString)
        {
            case "SSTA":
                returnEvent.EventType = EventType.SessionStarted;
                returnEvent.EventDetails = null;
                break;
            case "SEND":
                returnEvent.EventType = EventType.SessionEnded;
                returnEvent.EventDetails = null;
                break;
            case "FTLP":
                returnEvent.EventType = EventType.FastestLap;
                returnEvent.EventDetails = new Event.FastestLap
                {
                    CarIndex = byteArrayManager.NextByte(),
                    //* 1000 for miliseconds
                    LapTime = BitConverter.ToSingle(byteArrayManager.NextBytes(4)) * 1000
                };
                break;
            case "RTMT":
                returnEvent.EventType = EventType.Retirement;
                returnEvent.EventDetails = new Event.Retirement
                {
                    CarIndex = byteArrayManager.NextByte()
                };
                break;
            case "DRSE":
                returnEvent.EventType = EventType.RcDRSEnabled;
                returnEvent.EventDetails = null;
                break;
            case "DRSD":
                returnEvent.EventType = EventType.RcDRSDisabled;
                returnEvent.EventDetails = null;
                break;
            case "TMPT":
                returnEvent.EventType = EventType.TeamMateInPits;
                returnEvent.EventDetails = new Event.TeamMateInPits
                {
                    CarIndex = byteArrayManager.NextByte()
                };
                break;
            case "CHQF":
                returnEvent.EventType = EventType.CheckeredFlagWaved;
                returnEvent.EventDetails = null;
                break;
            case "RCWN":
                returnEvent.EventType = EventType.RaceWinner;
                returnEvent.EventDetails = new Event.RaceWinner
                {
                    CarIndex = byteArrayManager.NextByte()
                };
                break;
            case "PENA":
                returnEvent.EventType = EventType.Penalty;
                returnEvent.EventDetails = new Event.Penalty
                {
                    PenaltyType = (PenaltyType)byteArrayManager.NextByte(),
                    InfringementType = (InfringementType)byteArrayManager.NextByte(),
                    CarIndex = byteArrayManager.NextByte(),
                    OtherCarIndex = byteArrayManager.NextByte(),
                    Time = byteArrayManager.NextByte(),
                    LapNumber = byteArrayManager.NextByte(),
                    PlacesGained = byteArrayManager.NextByte()
                };
                break;
            case "SPTP":
                returnEvent.EventType = EventType.SpeedTrap;
                var details = new Event.SpeedTrap
                {
                    CarIndex = byteArrayManager.NextByte(),
                    Speed = BitConverter.ToSingle(byteArrayManager.NextBytes(4)),
                    IsOverallFastestInSession = byteArrayManager.NextByte() == 1,
                };
                
                if (packageFormat == PackageFormat.F12021)
                {
                    returnEvent.EventDetails = details;
                    break;
                }

                details.IsDriverFastestInSession = byteArrayManager.NextByte() == 1;
                details.FastestCarIndex = byteArrayManager.NextByte();
                details.FastestSpeedInSession = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
                
                returnEvent.EventDetails = details;
                break;
            case "STLG":
                returnEvent.EventType = EventType.StartLights;
                returnEvent.EventDetails = new Event.StartLights
                {
                    NumberOfLights = byteArrayManager.NextByte()
                };
                break;
            case "LGOT":
                returnEvent.EventType = EventType.LightsOut;
                returnEvent.EventDetails = null;
                break;
            case "DTSV":
                returnEvent.EventType = EventType.DriveThroughPenaltyServed;
                returnEvent.EventDetails = new Event.DriveThroughPenaltyServed()
                {
                    CarIndex = byteArrayManager.NextByte()
                };
                break;
            case "SGSV":
                returnEvent.EventType = EventType.StopGoPenaltyServed;
                returnEvent.EventDetails = new Event.StopGoPenaltyServed()
                {
                    CarIndex = byteArrayManager.NextByte()
                };
                break;
            case "FLBK":
                returnEvent.EventType = EventType.Flashback;
                returnEvent.EventDetails = new Event.Flashback
                {
                    FlashbackFrameIdentifier = BitConverter.ToUInt32(byteArrayManager.NextBytes(4)),
                    FlashBackSessionTime = BitConverter.ToSingle(byteArrayManager.NextBytes(4))
                };
                break;
            case "BUTN":
                returnEvent.EventType = EventType.Buttons;
                returnEvent.EventDetails = new Event.Buttons()
                {
                    ButtonStatus = BitConverter.ToUInt32(byteArrayManager.NextBytes(4))
                };
                break;
            default:
                returnEvent.EventType = EventType.Unknown;
                returnEvent.EventDetails = null;
                break;
        }

        return returnEvent;
    }
}