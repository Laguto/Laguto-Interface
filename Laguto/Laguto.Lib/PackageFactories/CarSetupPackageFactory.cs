﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class CarSetupPackageFactory : AbstractFactory<CarSetup>
{
    public override CarSetup CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);
        var carSetupData = new CarSetup.CarSetupData[22];

        for (var i = 0; i < 22; i++)
        {
            carSetupData[i] = ParseSingle(byteArrayManager);
        }

        return new CarSetup
        {
            FieldSetupData = carSetupData
        };
    }

    public CarSetup.CarSetupData ParseSingle(ByteArrayManager byteArrayManager)
    {
            var frontWing = byteArrayManager.NextByte();
            var rearWing  = byteArrayManager.NextByte();
            var differentialOnThrottle  = byteArrayManager.NextByte();
            var differentialOffThrottle  = byteArrayManager.NextByte();
            var frontCamber  = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var rearCamber = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var frontToe = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var rearToe = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var frontSuspension = byteArrayManager.NextByte();
            var rearSuspension = byteArrayManager.NextByte();
            var frontAntiRollBar = byteArrayManager.NextByte();
            var rearAntiRollBar = byteArrayManager.NextByte();
            var frontSuspensionHeight = byteArrayManager.NextByte();
            var rearSuspensionHeight = byteArrayManager.NextByte();
            var brakePressure = byteArrayManager.NextByte();
            var brakeBias = byteArrayManager.NextByte();
            var rearLeftTyrePressure = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var rearRightTyrePressure = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var frontLeftTyrePressure = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var frontRightTyrePressure = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var ballast = byteArrayManager.NextByte();
            var fuelLoad  = byteArrayManager.NextByte();

            return new CarSetup.CarSetupData
            {
                Ballast = ballast,
                BrakeBias = brakeBias,
                BrakePressure = brakePressure,
                FrontCamber = frontCamber,
                FrontSuspension = frontSuspension,
                FrontToe = frontToe,
                FrontWing = frontWing,
                FuelLoad = fuelLoad,
                RearCamber = rearCamber,
                RearSuspension = rearSuspension,
                RearToe = rearToe,
                RearWing = rearWing,
                DifferentialOffThrottle = differentialOffThrottle,
                DifferentialOnThrottle = differentialOnThrottle,
                FrontSuspensionHeight = frontSuspensionHeight,
                RearSuspensionHeight = rearSuspensionHeight,
                FrontAntiRollBar = frontAntiRollBar,
                FrontLeftTyrePressure = frontLeftTyrePressure,
                FrontRightTyrePressure = frontRightTyrePressure,
                RearAntiRollBar = rearAntiRollBar,
                RearLeftTyrePressure = rearLeftTyrePressure,
                RearRightTyrePressure = rearRightTyrePressure
            };
    }
}