﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class LapPackageFactory : AbstractFactory<Lap>
{
    public override Lap CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var lapData = new Lap.LapData[22];
        for (int i = 0; i < lapData.Length; i++)
        {
            lapData[i] = ParseSingle(byteArrayManager);
        }

        if (packageFormat != PackageFormat.F12022)
            return new Lap
            {
                FieldLapData = lapData,
            };
        
        var timeTrialPbCarIndex = byteArrayManager.NextByte();
        var timeTrialRivalCarIndex = byteArrayManager.NextByte();
            
        return new Lap
        {
            FieldLapData = lapData,
            TimeTrialPbCarIndex = timeTrialPbCarIndex,
            TimeTrialRivalCarIndex = timeTrialRivalCarIndex
        };

    }

    public Lap.LapData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var lastLapTime = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
            var currentLapTime = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
            var sector1Time = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
            var sector2Time = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
            var lapDistance = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var totalDistance = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var safetyCarDelta = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var carPosition = byteArrayManager.NextByte();
            var currentLapNumber = byteArrayManager.NextByte();
            var pitStatus = (PitStatus)byteArrayManager.NextByte();
            var numberOfPitStops = byteArrayManager.NextByte();
            var sector = byteArrayManager.NextByte();
            var currentLapInvalid = byteArrayManager.NextByte() == 1;
            var penalties = byteArrayManager.NextByte();
            var warnings = byteArrayManager.NextByte();
            var numberOfUnservedDriveThroughPenalties = byteArrayManager.NextByte();
            var numberOfUnservedStopGoPenalties = byteArrayManager.NextByte();
            var startGridPosition = byteArrayManager.NextByte();
            var driverStatus = (DriverStatus)byteArrayManager.NextByte();
            var resultStatus = (ResultStatus)byteArrayManager.NextByte();
            var pitLaneTimerActive = byteArrayManager.NextByte() == 1;
            var pitLaneTimeInLane = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
            var pitStopTimer = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
            var pitStopShouldServePen = byteArrayManager.NextByte() == 1;
            
            return new Lap.LapData
            {
                LastLapTimeInMs = lastLapTime,
                CurrentLapTimeInMs = currentLapTime,
                Sector1TimeInMs = sector1Time,
                Sector2TimeInMs = sector2Time,
                LapDistance = lapDistance,
                TotalDistance = totalDistance,
                SafetyCarDelta = safetyCarDelta,
                CarPosition = carPosition,
                CurrentLapNumber = currentLapNumber,
                CurrentPitStatus = pitStatus,
                NumberPitStops = numberOfPitStops,
                Sector = sector,
                CurrentLapInvalid = currentLapInvalid,
                Penalties = penalties,
                Warnings = warnings,
                NumberUnservedDriveThroughPenalties = numberOfUnservedDriveThroughPenalties,
                NumberUnservedStopGoPenalties = numberOfUnservedStopGoPenalties,
                StartingGridPosition = startGridPosition,
                CurrentDriverStatus = driverStatus,
                FinalResultStatus = resultStatus,
                PitLaneTimerActive = pitLaneTimerActive,
                PitLaneTimeInLaneInMs = pitLaneTimeInLane,
                PitStopTimerInMs = pitStopTimer,
                PitStopShouldServePenalty = pitStopShouldServePen
            };
    }
}