﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class CarStatusPackageFactory : AbstractFactory<CarStatus>
{
    public override CarStatus CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);
        var carStatusData = new CarStatus.CarStatusData[22];

        for (var i = 0; i < 22; i++)
        {
            carStatusData[i] = ParseSingle(byteArrayManager);
        }

        return new CarStatus
        {
            FieldCarStatusData = carStatusData
        };
    }

    public CarStatus.CarStatusData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var tractionControlStatus = (TractionControlLevel)byteArrayManager.NextByte();
            
            //true is on, false is off
            var antiLockBrakesOn = byteArrayManager.NextByte() == 1;
            
            var selectedFuelMix = (FuelMix) byteArrayManager.NextByte();
            var frontBrakeBiasPercentage = byteArrayManager.NextByte();
            
            //true is on, false is off
            var pitLimiterOn = byteArrayManager.NextByte() == 1;

            var fuelLevel = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            var fuelCapacity = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            var fuelRemainingLaps = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            
            var maxRpm = BitConverter.ToUInt16(byteArrayManager.NextBytes(2),0);
            var idleRpm = BitConverter.ToUInt16(byteArrayManager.NextBytes(2),0);
            
            var maxGears = byteArrayManager.NextByte();
            
            //true is on, false is off
            var drsAllowed = byteArrayManager.NextByte() == 1;
            var drsActivationDistance = BitConverter.ToUInt16(byteArrayManager.NextBytes(2),0);
            
            
            var equippedTyreCompoundId = byteArrayManager.NextByte();
            var equippedVisualTyreCompound = (TyreCompound) byteArrayManager.NextByte();
            
            var tyreAgeLaps = byteArrayManager.NextByte();
            var vehicleFiaFlag = (FiaFlag) byteArrayManager.NextByte();
            
            var ersStoredEnergyJoules = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            var selectedErsDeployMode = (ErsDeployMode) byteArrayManager.NextByte();
            
            var ersHarvestedThisLapByMguk = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            var ersHarvestedThisLapByMguh = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);
            var ersDeployedThisLap = BitConverter.ToSingle(byteArrayManager.NextBytes(4),0);

            //true is yes, false is no
            var networkPaused = byteArrayManager.NextByte() == 1;

            return new CarStatus.CarStatusData
            {
                DrsAllowed = drsAllowed,
                FuelCapacity = fuelCapacity,
                FuelLevel = fuelLevel,
                IdleRpm = idleRpm,
                MaxGears = maxGears,
                MaxRpm = maxRpm,
                NetworkPaused = networkPaused,
                DrsActivationDistance = drsActivationDistance,
                FuelRemainingLaps = fuelRemainingLaps,
                PitLimiterOn = pitLimiterOn,
                SelectedFuelMix = selectedFuelMix,
                TractionControlStatus = tractionControlStatus,
                TyreAgeLaps = tyreAgeLaps,
                VehicleFiaFlag = vehicleFiaFlag,
                AntiLockBrakesOn = antiLockBrakesOn,
                EquippedTyreCompoundId = equippedTyreCompoundId,
                EquippedVisualTyreCompound = equippedVisualTyreCompound,
                ErsDeployedThisLap = ersDeployedThisLap,
                ErsStoredEnergyJoules = ersStoredEnergyJoules,
                FrontBrakeBiasPercentage = frontBrakeBiasPercentage,
                SelectedErsDeployMode = selectedErsDeployMode,
                ErsHarvestedThisLapByMGUH = ersHarvestedThisLapByMguh,
                ErsHarvestedThisLapByMGUK = ersHarvestedThisLapByMguk
            };
    }
}