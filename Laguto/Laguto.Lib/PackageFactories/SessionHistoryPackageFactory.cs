﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class SessionHistoryPackageFactory : AbstractFactory<SessionHistory>
{
    public override SessionHistory CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var carIndex = byteArrayManager.NextByte();
        var numberOfLaps = byteArrayManager.NextByte();
        var numberOfTyreStints = byteArrayManager.NextByte();
        var bestLapTimeLapNumber = byteArrayManager.NextByte();
        var bestSector1LapNumber = byteArrayManager.NextByte();
        var bestSector2LapNumber = byteArrayManager.NextByte();
        var bestSector3LapNumber = byteArrayManager.NextByte();

        var lapHistoryData = new SessionHistory.LapHistory[100];
        for(int i = 0; i < 100; i++)
        {
            lapHistoryData[i] = ParseSingleLapHistory(byteArrayManager);
        }

        var tyreStintHistory = new SessionHistory.TyreStintHistory[8];
        for(int i = 0; i < 8; i++)
        {
            tyreStintHistory[i] = ParseSingleTyreStintHistory(byteArrayManager);
        }

        return new SessionHistory
        {
            CarIndex = carIndex,
            NumberOfLaps = numberOfLaps,    
            NumberOfTyreStints = numberOfTyreStints,
            BestLapTimeLapNumber = bestLapTimeLapNumber,
            BestSector1LapNumber = bestSector1LapNumber,
            BestSector2LapNumber = bestSector2LapNumber,
            BestSector3LapNumber = bestSector3LapNumber,
            LapHistoryData = lapHistoryData,
            TyreStintHistoryData = tyreStintHistory
        };
    }

    public SessionHistory.LapHistory ParseSingleLapHistory(ByteArrayManager byteArrayManager)
    {
        var lapTime = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
        var sector1Time = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var sector2Time = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var sector3Time = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var lapValidBitFlags = byteArrayManager.NextByte();
        
        return new SessionHistory.LapHistory
        {
            LapTimeInMs = lapTime,
            Sector1TimeInMs = sector1Time,
            Sector2TimeInMs = sector2Time,
            Sector3TimeInMs = sector3Time,
            LapValidBitFlags = lapValidBitFlags
        };
    }

    public SessionHistory.TyreStintHistory ParseSingleTyreStintHistory(ByteArrayManager byteArrayManager)
    {
        var endLap = byteArrayManager.NextByte();
        var tyreActualCompound = byteArrayManager.NextByte();
        var tyreVisualCompound = byteArrayManager.NextByte();
        
        return new SessionHistory.TyreStintHistory
        {
            EndLap = endLap,
            TyreActualCompound = tyreActualCompound,
            TyreVisualCompound = tyreVisualCompound
        };
    }
}