﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class MotionPackageFactory : AbstractFactory<Motion>
{
    public override Motion CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var carMotionData = new Motion.CarMotionData[22];
        for(int i = 0; i < 22; i++)
        {
            carMotionData[i] = ParseSingle(byteArrayManager);
        }

        var suspensionPosition = new float[4];
        for(int i = 0; i < 4; i++)
            suspensionPosition[i] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var suspensionVelocity = new float[4];
        for (int i = 0; i < 4; i++)
            suspensionVelocity[i] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var suspensionAcceleration = new float[4];
        for (int i = 0; i < 4; i++)
            suspensionAcceleration[i] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var wheelSpeed = new float[4];
        for (int i = 0; i < 4; i++)
            wheelSpeed[i] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var wheelSlip = new float[4];
        for (int i = 0; i < 4; i++)
            wheelSlip[i] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var localVelocityX = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var localVelocityY = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var localVelocityZ = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var angularVelocityX = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var angularVelocityY = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var angularVelocityZ = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var angularAccelerationX = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var angularAccelerationY = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var angularAccelerationZ = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        var frontWheelsAngle = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

        return new Motion
        {
            FieldMotionData = carMotionData,
            SuspensionPosition = suspensionPosition,
            SuspensionVelocity = suspensionVelocity,
            SuspensionAcceleration = suspensionAcceleration,
            WheelSpeed = wheelSpeed,
            WheelSlip = wheelSlip,
            LocalVelocityX = localVelocityX,
            LocalVelociytY = localVelocityY,
            LocalVelocityZ = localVelocityZ,
            AngularVelocityX = angularVelocityX,
            AngularVelocityY = angularVelocityY,
            AngularVelocityZ = angularVelocityZ,
            AngularAccelerationX = angularAccelerationX,
            AngularAccelerationY = angularAccelerationY,
            AngularAccelerationZ = angularAccelerationZ,
            FrontWheelsAngle = frontWheelsAngle
        };
    }

    public Motion.CarMotionData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var worldPositionX = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var worldPositionY = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var worldPositionZ = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

            var worldVelocityX = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var worldVelocityY = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var worldVelocityZ = BitConverter.ToSingle(byteArrayManager.NextBytes(4));

            var worldForwardDirX = BitConverter.ToInt16(byteArrayManager.NextBytes(2));
            var worldForwardDirY = BitConverter.ToInt16(byteArrayManager.NextBytes(2));
            var worldForwardDirZ = BitConverter.ToInt16(byteArrayManager.NextBytes(2));

            var worldRightDirX = BitConverter.ToInt16(byteArrayManager.NextBytes(2));
            var worldRightDirY = BitConverter.ToInt16(byteArrayManager.NextBytes(2));
            var worldRightDirZ = BitConverter.ToInt16(byteArrayManager.NextBytes(2));

            var gForceLateral = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var gForceLongitudinal = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var gForceVertical = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var yaw = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var pitch = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            var roll = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
            
            return new Motion.CarMotionData
            {
                WorldPositionX = worldPositionX,
                WorldPositionY = worldPositionY,
                WorldPositionZ = worldPositionZ,
                WorldVelocityX = worldVelocityX,
                WorldVelocityY = worldVelocityY,
                WorldVelocityZ = worldVelocityZ,
                WorldForwardDirX = worldForwardDirX,
                WorldForwardDirY = worldForwardDirY,
                WorldForwardDirZ = worldForwardDirZ,
                WorldRightDirX = worldRightDirX,
                WorldRightDirY = worldRightDirY,
                WorldRightDirZ = worldRightDirZ,
                GForceLateral = gForceLateral,
                GForceLongitudinal = gForceLongitudinal,
                GForceVertical = gForceVertical,
                Yaw = yaw,
                Pitch = pitch,
                Roll = roll
            };
    }
}