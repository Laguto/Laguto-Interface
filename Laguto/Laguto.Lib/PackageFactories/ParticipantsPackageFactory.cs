﻿using System.Text;
using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class ParticipantsPackageFactory : AbstractFactory<Participants>
{
    public override Participants CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var numberOfActiveCars = byteArrayManager.NextByte();

        var participantData = new Participants.ParticipantData[22];
        
        for(int i = 0; i < 22; i++)
        {
            participantData[i] = ParseSingle(byteArrayManager);
        }

        return new Participants
        {
            NumberOfActiveCars = numberOfActiveCars,
            FieldParticipantData = participantData
        };
    }

    public Participants.ParticipantData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var isAiControlled = byteArrayManager.NextByte() == 1;
        var driverId = byteArrayManager.NextByte();
        var networkId = byteArrayManager.NextByte();
        var teamId = (Team) byteArrayManager.NextByte();
        var myTeam = byteArrayManager.NextByte() == 1;
        var raceNumber = byteArrayManager.NextByte();
        var nationality = byteArrayManager.NextByte();
        var name = Encoding.UTF8.GetString(byteArrayManager.NextBytes(48).TakeWhile(b => !b.Equals(0)).ToArray());
        var yourTelemetry = byteArrayManager.NextByte() == 1;
       
        return new Participants.ParticipantData
        {
            IsAiControlled = isAiControlled,
            DriverId = driverId,
            NetworkId = networkId,
            ManufacturingTeam = teamId,
            MyTeam = myTeam,
            CarRaceNumber = raceNumber,
            NationalityId = nationality,
            Name = name,
            TelemetryPublic = yourTelemetry
        };
    }
}