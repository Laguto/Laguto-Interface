﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class CarTelemetryPackageFactory : AbstractFactory<CarTelemetry>
{
    public override CarTelemetry CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);
        var carTelemetryData = new CarTelemetry.CarTelemetryData[22];

        
        for (var i = 0; i < 22; i++)
        {
            carTelemetryData[i] = ParseSingle(byteArrayManager);
        }

        var mfdPanelIndex = byteArrayManager.NextByte();
        var mfdPanelIndexSecondPlayer = byteArrayManager.NextByte();

        var suggestedGear = byteArrayManager.NextByte();

        return new CarTelemetry
        {
            SuggestedGear = suggestedGear,
            FieldTelemetryData = carTelemetryData,
            MfdPanelIndex = mfdPanelIndex,
            MfdPanelIndexSecondaryPlayer = mfdPanelIndexSecondPlayer
        };
    }

    public CarTelemetry.CarTelemetryData ParseSingle(ByteArrayManager byteArrayManager)
    {
            var speed = BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);
            var throttle = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var steer = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var brake = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
            var clutch = byteArrayManager.NextByte();
            var gear = (sbyte) byteArrayManager.NextByte();
            var engineRpm = BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);
            var drs = byteArrayManager.NextByte();
            var revLightsPercent = byteArrayManager.NextByte();
            var revLightsBitValue = BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);

            var brakesTemperature = new ushort[4];
            for (int j = 0; j < 4; j++)
                brakesTemperature[j] = BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);

            var tyresSurfaceTemperature = new byte[4];
            for (int j = 0; j < 4; j++)
                tyresSurfaceTemperature[j] = byteArrayManager.NextByte();
            
            var tyresInnerTemperature = new byte[4];
            for (int j = 0; j < 4; j++)
                tyresInnerTemperature[j] = byteArrayManager.NextByte();

            var engineTemperature = BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);
            
            var tyresPressure = new float[4];
            for (int j = 0; j < 4; j++)
                tyresPressure[j] = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);

            var surfaceType = new SurfaceType[4];
            for (int j = 0; j < 4; j++)
                surfaceType[j] = (SurfaceType) byteArrayManager.NextByte();

            return new CarTelemetry.CarTelemetryData
            {
                Brake = brake,
                Clutch = clutch,
                Gear = gear,
                Speed = speed,
                Steer = steer,
                Throttle = throttle,
                BrakesTemperature = brakesTemperature,
                EngineTemperature = engineTemperature,
                SurfaceType = surfaceType,
                TyresPressure = tyresPressure,
                DRS = drs,
                RevLightsPercent = revLightsPercent,
                TyresInnerTemperature = tyresInnerTemperature,
                TyresSurfaceTemperature = tyresSurfaceTemperature,
                EngineRPM = engineRpm,
                RevLightsBitValue = revLightsBitValue
            };
    }
}