﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class HeaderPackageFactory : AbstractFactory<Header>
{
    public override Header CreateFromBytes(byte[] bytes, PackageFormat packageFormat = PackageFormat.F12021)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        
        var packetFormat = (PackageFormat) BitConverter.ToUInt16(byteArrayManager.NextBytes(2), 0);

        //Version bytes
        var majorVersion = byteArrayManager.NextByte();
        var minorVersion = byteArrayManager.NextByte();

        //Packet version bytes
        var packetVersion = byteArrayManager.NextByte();

        //Packet bytes
        var packetTypeByte = byteArrayManager.NextByte();
        PacketType packetType = PacketType.Unknown;

        switch (packetTypeByte)
        {
           case 0 : 
               packetType = PacketType.Motion;
               break;
           case 1 :
               packetType = PacketType.Session;
               break;
           case 2:
               packetType = PacketType.Lap;
               break;
           case 3 : 
               packetType = PacketType.Event;
               break;
           case 4 :
               packetType = PacketType.Participants;
               break;
           case 5:
               packetType = PacketType.CarSetup;
               break;
           case 6 : 
               packetType = PacketType.CarTelemetry;
               break;
           case 7 :
               packetType = PacketType.CarStatus;
               break;
           case 8:
               packetType = PacketType.FinalClassification;
               break;
           case 9 : 
               packetType = PacketType.LobbyInfo;
               break;
           case 10 :
               packetType = PacketType.CarDamage;
               break;
           case 11:
               packetType = PacketType.SessionHistory;
               break;
        }
        
        var uniqueSessionId = BitConverter.ToUInt64(byteArrayManager.NextBytes(8), 0);
        
        var sessionTime = BitConverter.ToSingle(byteArrayManager.NextBytes(4), 0);
        
        var frameIdentifier = BitConverter.ToUInt32(byteArrayManager.NextBytes(4), 0);

        var playerCarIndex = byteArrayManager.NextByte();

        var secondaryPlayerCarIndex = byteArrayManager.NextByte();

        return new Header()
        {
            FrameIdentifier = frameIdentifier,
            GameMajorVersion = majorVersion,
            GameMinorVersion = minorVersion,
            PackageFormat = packetFormat,
            PacketType = packetType,
            PacketVersion = packetVersion,
            PlayerCarIndex = playerCarIndex,
            SessionTime = sessionTime,
            SecondaryPlayerCarIndex = secondaryPlayerCarIndex,
            UniqueSessionId = uniqueSessionId
        };
    }
}