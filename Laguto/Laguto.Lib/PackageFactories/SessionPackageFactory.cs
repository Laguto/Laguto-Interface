﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class SessionPackageFactory : AbstractFactory<Session>
{
    public override Session CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var weather = (WeatherCondition)byteArrayManager.NextByte();
        var trackTemperature = byteArrayManager.NextByte();
        var airTemperature = byteArrayManager.NextByte();
        var totalLaps = byteArrayManager.NextByte();
        var trackLength = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var sessionType = (SessionType)byteArrayManager.NextByte();
        var track = (Track)byteArrayManager.NextByte();
        var formula = (FormulaType)byteArrayManager.NextByte();
        var sessionTimeLeft = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var sessionDuration = BitConverter.ToUInt16(byteArrayManager.NextBytes(2));
        var pitSpeedLimit = byteArrayManager.NextByte();
        var gamePaused = byteArrayManager.NextByte() == 1;
        var isSpectating = byteArrayManager.NextByte() == 1;
        var spectatorCarIndex = byteArrayManager.NextByte();
        var sliProNativeSupport = byteArrayManager.NextByte() == 1;
        var numberOfMarshalZones = byteArrayManager.NextByte();
        
        var marshalZones = new MarshallZone[21];
        for(int i = 0; i < 21; i++)
        {
            marshalZones[i] = ParseSingleMarshallZone(byteArrayManager);
        }

        var safetyCarStatus = (SafetyCarStatus)byteArrayManager.NextByte();
        var networkGame = byteArrayManager.NextByte() == 1;
        var numberOfWeatherForecastSamples = byteArrayManager.NextByte();
        
        var weatherForecastSamples = new WeatherForecastSample[56];
        for(int i = 0; i < 56; i++)
        {
            weatherForecastSamples[i] = ParseSingleWeatherForecastSample(byteArrayManager);
        }

        var isForecastAccurate = byteArrayManager.NextByte() == 1;
        var aiDifficulty = byteArrayManager.NextByte();
        var seasonLinkIdentifier = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
        var weekendLinkIdentifier = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
        var sessionLinkIdentifier = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));

        var pitStopWindowIdealLap = byteArrayManager.NextByte();
        var pitStopWindowLatestLap = byteArrayManager.NextByte();
        var pitStopRejoinPosition = byteArrayManager.NextByte();
        var steeringAssist = byteArrayManager.NextByte() == 1;
        var breakingAssist = byteArrayManager.NextByte();
        var gearboxAssist = byteArrayManager.NextByte();
        var pitAssist = byteArrayManager.NextByte() == 1;
        var pitReleaseAssist = byteArrayManager.NextByte() == 1;
        var ersAssist = byteArrayManager.NextByte() == 1;
        var drsAssist = byteArrayManager.NextByte() == 1;
        var dynamicRacingLine = byteArrayManager.NextByte();
        var dynamicRacingLineType = byteArrayManager.NextByte() == 1;
        
        var session = new Session
        {
            CurrentWeatherCondition = weather,
            TrackTemperatureCelsius = (sbyte)trackTemperature,
            AirTemperatureCelsius = (sbyte)airTemperature,
            TotalLapsInRace = totalLaps,
            TrackLengthMeters = trackLength,
            SessionTypeMode = sessionType,
            SessionTrack = track,
            Formula = formula,
            SessionTimeLeft = sessionTimeLeft,
            SessionDuration = sessionDuration,
            PitSpeedLimitKph = pitSpeedLimit,
            GamePaused = gamePaused,
            IsSpectating = isSpectating,
            SpectatedCarIndex = spectatorCarIndex,
            SliProNativeSupport = sliProNativeSupport,
            NumberOfMarshallZones = numberOfMarshalZones,
            MarshallZones = marshalZones,
            CurrentSafetyCarStatus = safetyCarStatus,
            IsNetworkGame = networkGame,
            NumberOfWeatherForecastSamples = numberOfWeatherForecastSamples,
            WeatherForecastSamples = weatherForecastSamples,
            ForecastIsAccurate = isForecastAccurate,
            AiDifficulty = aiDifficulty,
            SeasonLinkIdentifier = seasonLinkIdentifier,
            WeekendLinkIdentifier = weekendLinkIdentifier,
            SessionLinkIdentifier = sessionLinkIdentifier,
            PitStopWindowIdealLap = pitStopWindowIdealLap,
            PitStopWindowLatestLap = pitStopWindowLatestLap,
            PitStopRejoinPosition = pitStopRejoinPosition,
            SteeringAssist = steeringAssist,
            BreakingAssist = breakingAssist,
            GearboxAssist = gearboxAssist,
            PitAssist = pitAssist,
            PitReleaseAssist = pitReleaseAssist,
            ErsAssist = ersAssist,
            DrsAssist = drsAssist,
            DynanicRacingLine = dynamicRacingLine,
            DynamicRacingLineType3D = dynamicRacingLineType,
        };

        if (packageFormat == PackageFormat.F12021)
            return session;
        
        var gameMode = (GameMode)byteArrayManager.NextByte();
        var ruleSet = (RuleSet)byteArrayManager.NextByte();
        var timeOfDay = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
        var sessionLength = (SessionLength)byteArrayManager.NextByte();

        session.GameMode = gameMode;
        session.RuleSet = ruleSet;
        session.TimeOfDay = timeOfDay;
        session.SessionLength = sessionLength;

        return session;
    }

    public MarshallZone ParseSingleMarshallZone(ByteArrayManager byteArrayManager)
    {
        var zoneStart = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        var zoneFlag = (FiaFlag)byteArrayManager.NextByte();
        return new MarshallZone
        {
            ZoneStart = zoneStart,
            ZoneFlag = zoneFlag
        };
    }

    public WeatherForecastSample ParseSingleWeatherForecastSample(ByteArrayManager byteArrayManager)
    {
        var w_sessionType = (SessionType)byteArrayManager.NextByte();
        var w_timeOffset = byteArrayManager.NextByte();
        var w_weather = (WeatherCondition)byteArrayManager.NextByte();
        var w_trackTemperature = byteArrayManager.NextByte();
        var w_trackTemperatureChange = byteArrayManager.NextByte();
        var w_airTemperature = byteArrayManager.NextByte();
        var w_airTemperatureChange = byteArrayManager.NextByte();
        var w_rainPercentage = byteArrayManager.NextByte();

        return new WeatherForecastSample
        {
            SessionTypeMode = w_sessionType,
            TimeOffSet = w_timeOffset,
            ForecastedWeatherCondition = w_weather,
            TrackTemperatureCelsius = (sbyte)w_trackTemperature,
            TrackTemperatureChange = (sbyte)w_trackTemperatureChange,
            AirTemperatureCelsius = (sbyte)w_airTemperature,
            AirTemperatureChange = (sbyte)w_airTemperatureChange,
            RainPercentage = w_rainPercentage
        };
    }
}