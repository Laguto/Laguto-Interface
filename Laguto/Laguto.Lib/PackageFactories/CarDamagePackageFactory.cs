﻿using System.Text.Json;
using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using Newtonsoft.Json;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class CarDamagePackageFactory : AbstractFactory<CarDamage>
{
    public override CarDamage CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);
        var carDamageData = new CarDamage.CarDamageData[22];

        for (var i = 0; i < 22; i++)
        {
            switch (packageFormat)
            {
                case PackageFormat.F12021:
                    carDamageData[i] = ParseSingle2021(byteArrayManager);
                    break;
                case PackageFormat.F12022:
                    carDamageData[i] = ParseSingle(byteArrayManager);
                    break;
            }
        }
        
        return new CarDamage()
        {
            FieldCarStatusData = carDamageData
        };
    }

    public CarDamage.CarDamageData ParseSingle2021(ByteArrayManager byteArrayManager)
    {
            var tyreWear = new float[4];
            for (int j = 0; j < 4; j++)
                tyreWear[j] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        
            var tyreDamage = byteArrayManager.NextBytes(4);

            var breakDamage = byteArrayManager.NextBytes(4);

            //Wing damages
            var frontLeftWingDamage = byteArrayManager.NextByte();
            var frontRightWingDamage = byteArrayManager.NextByte();
            var rearWingDamage = byteArrayManager.NextByte();

            var floorDamage = byteArrayManager.NextByte();

            var diffuserDamage = byteArrayManager.NextByte();

            var sidepodDamage = byteArrayManager.NextByte();

            //true if fault, false if not
            var drsFault = byteArrayManager.NextByte() == 1;

            var gearBoxDamage = byteArrayManager.NextByte();
        
            //Engine damages
            var engineDamage = byteArrayManager.NextByte();
            var engineMguhWear= byteArrayManager.NextByte();
            var engineEsWear = byteArrayManager.NextByte();
            var engineCeWear = byteArrayManager.NextByte();
            var engineIceWear = byteArrayManager.NextByte();
            var engineMgukWear= byteArrayManager.NextByte();
            var engineTcWear = byteArrayManager.NextByte();

            return new CarDamage.CarDamageData
            {
                BrakesDamage = breakDamage,
                DiffuserDamage = diffuserDamage,
                DrsFault = drsFault,
                EngineCeWear = engineCeWear,
                EngineDamage = engineDamage,
                EngineEsWear = engineEsWear,
                EngineIceWear = engineIceWear,
                EngineMguhWear = engineMguhWear,
                EngineMgukWear = engineMgukWear,
                EngineTcWear = engineTcWear,
                FloorDamage = floorDamage,
                SidepodDamage = sidepodDamage,
                TyresDamage = tyreDamage,
                TyreWear = tyreWear,
                GearBoxDamage = gearBoxDamage,
                RearWingDamage = rearWingDamage,
                FrontLeftWingDamage = frontLeftWingDamage,
                FrontRightWingDamage = frontRightWingDamage
            };
    }

    public CarDamage.CarDamageData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var tyreWear = new float[4];
            for (int j = 0; j < 4; j++)
                tyreWear[j] = BitConverter.ToSingle(byteArrayManager.NextBytes(4));
        
            var tyreDamage = byteArrayManager.NextBytes(4);

            var breakDamage = byteArrayManager.NextBytes(4);

            //Wing damages
            var frontLeftWingDamage = byteArrayManager.NextByte();
            var frontRightWingDamage = byteArrayManager.NextByte();
            var rearWingDamage = byteArrayManager.NextByte();

            var floorDamage = byteArrayManager.NextByte();

            var diffuserDamage = byteArrayManager.NextByte();

            var sidepodDamage = byteArrayManager.NextByte();

            //true if fault, false if not
            var drsFault = byteArrayManager.NextByte() == 1;

            var ersFault = byteArrayManager.NextByte() == 1;
        
            var gearBoxDamage = byteArrayManager.NextByte();
        
            //Engine damages
            var engineDamage = byteArrayManager.NextByte();
            var engineMguhWear= byteArrayManager.NextByte();
            var engineEsWear = byteArrayManager.NextByte();
            var engineCeWear = byteArrayManager.NextByte();
            var engineIceWear = byteArrayManager.NextByte();
            var engineMgukWear= byteArrayManager.NextByte();
            var engineTcWear = byteArrayManager.NextByte();
        

            //true if fault, false if not
            var engineBlown = byteArrayManager.NextByte() == 1;
        
            //true if fault, false if not
            var engineSeized = byteArrayManager.NextByte() == 1;

            return new CarDamage.CarDamageData
            {
                BrakesDamage = breakDamage,
                DiffuserDamage = diffuserDamage,
                DrsFault = drsFault,

                EngineCeWear = engineCeWear,
                EngineDamage = engineDamage,
                EngineEsWear = engineEsWear,
                EngineIceWear = engineIceWear,
                EngineMguhWear = engineMguhWear,
                EngineMgukWear = engineMgukWear,
                EngineTcWear = engineTcWear,
                FloorDamage = floorDamage,
                SidepodDamage = sidepodDamage,
                TyresDamage = tyreDamage,
                TyreWear = tyreWear,
                GearBoxDamage = gearBoxDamage,
                RearWingDamage = rearWingDamage,
                FrontLeftWingDamage = frontLeftWingDamage,
                FrontRightWingDamage = frontRightWingDamage,
                EngineBlown = engineBlown,
                EngineSeized = engineSeized,
                ErsFault = ersFault
            };
    }
}
