﻿using System.Text;
using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class LobbyPackageFactory : AbstractFactory<Lobby>
{
    public override Lobby CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);
        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var numberOfPlayers = byteArrayManager.NextByte();

        var lobbyInfoData = new Lobby.LobbyData[22];

        for (var i = 0; i < 22; i++)
        {
            lobbyInfoData[i] = ParseSingle(byteArrayManager);
        }

        return new Lobby
        {
            LobbyPlayers = lobbyInfoData,
            NumberOfPlayers = numberOfPlayers
        };
    }

    public Lobby.LobbyData ParseSingle(ByteArrayManager byteArrayManager)
    {
        //True if AI, false if Human
        var aiControlled = byteArrayManager.NextByte() == 1;

        var teamId = (Team) byteArrayManager.NextByte();

        var nationality = byteArrayManager.NextByte();

        //https://stackoverflow.com/questions/144176/fastest-way-to-convert-a-possibly-null-terminated-ascii-byte-to-a-string
        var driverName = Encoding.UTF8.GetString(byteArrayManager.NextBytes(48).TakeWhile(b => !b.Equals(0)).ToArray());

        var carNumber = byteArrayManager.NextByte();

        var readyStatus = byteArrayManager.NextByte();

        return new Lobby.LobbyData
        {
            Name = driverName,
            Team = teamId,
            CarNumber = carNumber,
            NationalityId = nationality,
            IsAiControlled = aiControlled,
            ReadyStatusId = readyStatus
        };
    }
}