﻿using Laguto.Lib.Enums;
using Laguto.Lib.Models;
using TimHanewich.Toolkit;

namespace Laguto.Lib.PackageFactories;

public class FinalClassificationPackageFactory : AbstractFactory<FinalClassification>
{
    public override FinalClassification CreateFromBytes(byte[] bytes, PackageFormat packageFormat)
    {
        ByteArrayManager byteArrayManager = new ByteArrayManager(bytes);

        byteArrayManager.NextBytes(Header.HEADER_LENGTH);

        var numberOfCars = byteArrayManager.NextByte();

        var classificationData = new FinalClassification.FinalClassificationData[22];
        for (int i = 0; i < 22; i++)
        {
            switch (packageFormat)
            {
                case PackageFormat.F12021:
                    classificationData[i] = ParseSingle2021(byteArrayManager);
                    break;
                case PackageFormat.F12022:
                    classificationData[i] = ParseSingle(byteArrayManager);
                    break;
            }
        }

        return new FinalClassification
        {
            NumCars = numberOfCars,
            FieldDriverData = classificationData
        };
    }

    public FinalClassification.FinalClassificationData ParseSingle2021(ByteArrayManager byteArrayManager)
    {
        var position = byteArrayManager.NextByte();
        var numberOfLaps = byteArrayManager.NextByte();
        var gridPosition = byteArrayManager.NextByte();
        var points = byteArrayManager.NextByte();
        var numberOfPitStops = byteArrayManager.NextByte();
        var resultStatus = (ResultStatus)byteArrayManager.NextByte();

        var bestLapTime = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
        var totalRaceTime = BitConverter.ToDouble(byteArrayManager.NextBytes(8));
        var penaltiesTime = byteArrayManager.NextByte();
        var numberOfPenalties = byteArrayManager.NextByte();
        var numberOfTyreStints = byteArrayManager.NextByte();
            
        var tyreStintsActual = new byte[8];
        for (int k = 0; k < 8; k++)
            tyreStintsActual[k] = byteArrayManager.NextByte();

        var tyreStintsVisual = new byte[8];
        for (int k = 0; k < 8; k++)
            tyreStintsVisual[k] = byteArrayManager.NextByte();
        
        return new FinalClassification.FinalClassificationData
        {
            Position = position,
            NumberOfLaps = numberOfLaps,
            GridPosition = gridPosition,
            Points = points,
            NumberOfPitStops = numberOfPitStops,
            ResultStatus = resultStatus,
            BestLapTimeInMs = bestLapTime,
            TotalRaceTimeInSeconds = totalRaceTime,
            PenaltiesTimeInSeconds = penaltiesTime,
            NumberOfPenalties = numberOfPenalties,
            NumberOfTyreStints = numberOfTyreStints,
            TyreStintsActual = tyreStintsActual,
            TyreStintsVisual = tyreStintsVisual
        };
    }

    public FinalClassification.FinalClassificationData ParseSingle(ByteArrayManager byteArrayManager)
    {
        var position = byteArrayManager.NextByte();
            var numberOfLaps = byteArrayManager.NextByte();
            var gridPosition = byteArrayManager.NextByte();
            var points = byteArrayManager.NextByte();
            var numberOfPitStops = byteArrayManager.NextByte();
            var resultStatus = (ResultStatus)byteArrayManager.NextByte();

            var bestLapTime = BitConverter.ToUInt32(byteArrayManager.NextBytes(4));
            var totalRaceTime = BitConverter.ToDouble(byteArrayManager.NextBytes(8));
            var penaltiesTime = byteArrayManager.NextByte();
            var numberOfPenalties = byteArrayManager.NextByte();
            var numberOfTyreStints = byteArrayManager.NextByte();
            
            var tyreStintsActual = new byte[8];
            for (int k = 0; k < 8; k++)
                tyreStintsActual[k] = byteArrayManager.NextByte();

            var tyreStintsVisual = new byte[8];
            for (int k = 0; k < 8; k++)
                tyreStintsVisual[k] = byteArrayManager.NextByte();

            var tyreStintsEndLaps = new byte[8];
            for (int k = 0; k < 8; k++)
                tyreStintsEndLaps[k] = byteArrayManager.NextByte();

            return new FinalClassification.FinalClassificationData
            {
                Position = position,
                NumberOfLaps = numberOfLaps,
                GridPosition = gridPosition,
                Points = points,
                NumberOfPitStops = numberOfPitStops,
                ResultStatus = resultStatus,
                BestLapTimeInMs = bestLapTime,
                TotalRaceTimeInSeconds = totalRaceTime,
                PenaltiesTimeInSeconds = penaltiesTime,
                NumberOfPenalties = numberOfPenalties,
                NumberOfTyreStints = numberOfTyreStints,
                TyreStintsActual = tyreStintsActual,
                TyreStintsVisual = tyreStintsVisual,
                TyreStintsEndLaps = tyreStintsEndLaps
            };
    }
}