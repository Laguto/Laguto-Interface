﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.PackageFactories;

public abstract class AbstractFactory<T>
{
    public abstract T CreateFromBytes(byte[] bytes, PackageFormat packageFormat);
}