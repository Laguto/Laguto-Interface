﻿using Newtonsoft.Json;

namespace Laguto.Lib.Models;

public abstract class AbstractPackage
{
    [JsonIgnore]
    public Header Header { get; set; }
}