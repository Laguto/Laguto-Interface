﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models;

public class CarStatus : AbstractPackage
{
    public CarStatusData[] FieldCarStatusData { get; set; }

    public class CarStatusData
    {
        public TractionControlLevel TractionControlStatus { get; set; }
        public bool AntiLockBrakesOn { get; set; }
        public FuelMix SelectedFuelMix { get; set; }
        public byte FrontBrakeBiasPercentage { get; set; }
        public bool PitLimiterOn { get; set; }
        public float FuelLevel { get; set; }
        public float FuelCapacity { get; set; }
        public float FuelRemainingLaps { get; set; }
        public ushort MaxRpm { get; set; }
        public ushort IdleRpm { get; set; }
        public byte MaxGears { get; set; }
        public bool DrsAllowed { get; set; }
        public ushort DrsActivationDistance { get; set; }
        public byte EquippedTyreCompoundId { get; set; }
        public TyreCompound EquippedVisualTyreCompound { get; set; }
        public byte TyreAgeLaps { get; set; }
        public FiaFlag VehicleFiaFlag { get; set; }
        public float ErsStoredEnergyJoules { get; set; }
        public ErsDeployMode SelectedErsDeployMode { get; set; }
        public float ErsHarvestedThisLapByMGUK { get; set; }
        public float ErsHarvestedThisLapByMGUH { get; set; }
        public float ErsDeployedThisLap { get; set; }
        public bool NetworkPaused { get; set; }
    }
}