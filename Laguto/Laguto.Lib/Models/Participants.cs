﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models;

public class Participants : AbstractPackage
{
    public byte NumberOfActiveCars { get; set; }
    public ParticipantData[] FieldParticipantData { get; set; }

    public class ParticipantData
    {
        public bool IsAiControlled { get; set; }
        //public Driver PilotingDriver { get; set; }
        public byte DriverId { get; set; }
        public byte NetworkId { get; set; }
        public Team ManufacturingTeam { get; set; }
        public bool MyTeam { get; set; }
        public byte CarRaceNumber { get; set; }
        public byte NationalityId { get; set; }
        public string Name { get; set; } // 48 char
        public bool TelemetryPublic { get; set; }
    }
}