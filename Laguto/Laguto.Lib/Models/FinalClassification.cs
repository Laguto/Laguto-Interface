﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models;

public class FinalClassification : AbstractPackage
{
    public byte NumCars { get; set; }
    public FinalClassificationData[] FieldDriverData { get; set; }
    
    public class FinalClassificationData
    {
        public byte Position { get; set; }
        public byte NumberOfLaps { get; set; }
        public byte GridPosition { get; set; }
        public byte Points { get; set; }
        public byte NumberOfPitStops { get; set; }
        public ResultStatus ResultStatus { get; set; }

        public uint BestLapTimeInMs { get; set; }
        public double TotalRaceTimeInSeconds { get; set; }
        public byte PenaltiesTimeInSeconds { get; set; }
        public byte NumberOfPenalties { get; set; }
        public byte NumberOfTyreStints { get; set; }
        public byte[] TyreStintsActual { get; set; }
        public byte[] TyreStintsVisual { get; set; }
        public byte[] TyreStintsEndLaps { get; set; }
    }
}