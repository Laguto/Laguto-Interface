﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class MarshallZone
    {
        public float ZoneStart { get; set; }
        public FiaFlag ZoneFlag { get; set; }
    }
}
