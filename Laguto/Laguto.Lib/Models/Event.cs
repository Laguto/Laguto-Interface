﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class Event : AbstractPackage
    {
        public EventType EventType { get; set; }
        public EventData EventDetails { get; set; }
        
        public abstract class EventData { }

        public class FastestLap : EventData
        {
            public byte CarIndex { get; set; }
            public float LapTime { get; set; }
        }

        public class Retirement : EventData
        {
            public byte CarIndex { get; set; }
        }

        public class TeamMateInPits : EventData
        {
            public byte CarIndex { get; set; }
        }

        public class RaceWinner : EventData
        {
            public byte CarIndex { get; set; }
        }

        public class Penalty : EventData
        {
            public PenaltyType PenaltyType { get; set; }
            public InfringementType InfringementType { get; set; }
            public byte CarIndex { get; set; }
            public byte OtherCarIndex { get; set; }
            public byte Time { get; set; }
            public byte LapNumber { get; set; }
            public byte PlacesGained { get; set; }
        }

        public class SpeedTrap : EventData
        {
            public byte CarIndex { get; set; }
            public float Speed { get; set; }
            public bool IsOverallFastestInSession { get; set; }
            public bool IsDriverFastestInSession { get; set; }
            public float FastestSpeedInSession { get; set; }
            public byte FastestCarIndex { get; set; }
        }

        public class StartLights : EventData
        {
            public byte NumberOfLights { get; set; }
        }

        public class DriveThroughPenaltyServed : EventData
        {
            public byte CarIndex { get; set; }
        }

        public class StopGoPenaltyServed : EventData
        {
            public byte CarIndex { get; set; }
        }

        public class Flashback : EventData
        {
            public uint FlashbackFrameIdentifier { get; set; }
            public float FlashBackSessionTime { get; set; }
        }

        public class Buttons : EventData
        {
            public uint ButtonStatus { get; set; }
        }
    }
}
