﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class Lobby : AbstractPackage
    {
        public byte NumberOfPlayers { get; set; }
        public LobbyData[] LobbyPlayers { get; set; }

        public class LobbyData
        {
            public bool IsAiControlled { get; set; }
            public Team Team { get; set; }
            public byte NationalityId { get; set; }
            public string Name { get; set; } // 48 Bytes
            public byte CarNumber { get; set; }
            public byte ReadyStatusId { get; set; }
        }
    }
}
