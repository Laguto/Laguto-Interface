﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models;

public class Lap : AbstractPackage
{
    public LapData[] FieldLapData { get; set; }

    public byte TimeTrialPbCarIndex { get; set; }
    public byte TimeTrialRivalCarIndex { get; set; }

    public class LapData
    {
        public uint LastLapTimeInMs { get; set; }
        public uint CurrentLapTimeInMs { get; set; }
        public ushort Sector1TimeInMs { get; set; }
        public ushort Sector2TimeInMs { get; set; }
        public float LapDistance { get; set; }
        public float TotalDistance { get; set; }
        public float SafetyCarDelta { get; set; }
        public byte CarPosition { get; set; }
        public byte CurrentLapNumber { get; set; }
        public PitStatus CurrentPitStatus { get; set; }
        public byte NumberPitStops { get; set; }
        public byte Sector { get; set; }
        public bool CurrentLapInvalid { get; set; }
        public byte Penalties { get; set; }
        public byte Warnings { get; set; }
        public byte NumberUnservedDriveThroughPenalties { get; set; }
        public byte NumberUnservedStopGoPenalties { get; set; }
        public byte StartingGridPosition { get; set; }
        public DriverStatus CurrentDriverStatus { get; set; }
        public ResultStatus FinalResultStatus { get; set; }
        
        public bool PitLaneTimerActive { get; set; }
        public ushort PitLaneTimeInLaneInMs { get; set; }
        public ushort PitStopTimerInMs { get; set; }
        public bool PitStopShouldServePenalty { get; set; }
        
    }
}