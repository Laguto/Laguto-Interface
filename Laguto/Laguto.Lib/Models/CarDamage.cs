﻿namespace Laguto.Lib.Models;

public class CarDamage : AbstractPackage
{
    public CarDamageData[] FieldCarStatusData;

    public class CarDamageData
    {
        public float[] TyreWear { get; set; }
        public byte[] TyresDamage { get; set; }
        public byte[] BrakesDamage { get; set; }
        public byte FrontLeftWingDamage { get; set; }
        public byte FrontRightWingDamage { get; set; }
        public byte RearWingDamage { get; set; }
        public byte FloorDamage { get; set; }
        public byte DiffuserDamage { get; set; }
        public byte SidepodDamage { get; set; }
        public bool DrsFault { get; set; }
        public bool ErsFault { get; set; }
        public byte GearBoxDamage { get; set; }
        public byte EngineDamage { get; set; }
        public byte EngineMguhWear { get; set; }
        public byte EngineEsWear { get; set; }
        public byte EngineCeWear { get; set; }
        public byte EngineIceWear { get; set; }
        public byte EngineMgukWear { get; set; }
        public byte EngineTcWear { get; set; }
        public bool EngineBlown { get; set; }
        public bool EngineSeized { get; set; }
    }
}