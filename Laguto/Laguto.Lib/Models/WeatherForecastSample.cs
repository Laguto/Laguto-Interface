﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class WeatherForecastSample
    {
        public SessionType SessionTypeMode { get; set; }
        public byte TimeOffSet { get; set; }
        public WeatherCondition ForecastedWeatherCondition { get; set; }
        public sbyte TrackTemperatureCelsius { get; set; }
        public sbyte TrackTemperatureChange { get; set; }
        public sbyte AirTemperatureCelsius { get; set; }
        public sbyte AirTemperatureChange { get; set; }
        public byte RainPercentage { get; set; }        
    }
}
