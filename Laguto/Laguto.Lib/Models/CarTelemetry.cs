﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class CarTelemetry : AbstractPackage
    {
        public CarTelemetryData[] FieldTelemetryData { get; set; }
        public byte MfdPanelIndex { get; set; }
        public byte MfdPanelIndexSecondaryPlayer { get; set; }
        public byte SuggestedGear { get; set; }

        public class CarTelemetryData
        {
            public ushort Speed { get; set; }
            public float Throttle { get; set; }
            public float Steer { get; set; }
            public float Brake { get; set; }
            public byte Clutch { get; set; }
            public sbyte Gear { get; set; }
            public ushort EngineRPM { get; set; }
            public byte DRS { get; set; }
            public byte RevLightsPercent { get; set; }
            public ushort RevLightsBitValue { get; set; }
            public ushort[] BrakesTemperature { get; set; }
            public byte[] TyresSurfaceTemperature { get; set; }
            public byte[] TyresInnerTemperature { get; set; }
            public ushort EngineTemperature { get; set; }
            public float[] TyresPressure { get; set; }
            public SurfaceType[] SurfaceType { get; set; }
        }
    }
}
