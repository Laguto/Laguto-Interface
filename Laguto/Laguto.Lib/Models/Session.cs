﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models;

public class Session : AbstractPackage
{
    public WeatherCondition CurrentWeatherCondition { get; set; }
    public sbyte TrackTemperatureCelsius { get; set; }
    public sbyte AirTemperatureCelsius { get; set; }
    public byte TotalLapsInRace { get; set; }
    public ushort TrackLengthMeters { get; set; }
    public SessionType SessionTypeMode { get; set; }
    public Track SessionTrack { get; set; }
    public FormulaType Formula { get; set; }
    public ushort SessionTimeLeft { get; set; }
    public ushort SessionDuration { get; set; }
    public byte PitSpeedLimitKph { get; set; }
    public bool GamePaused { get; set; }
    public bool IsSpectating { get; set; }
    public byte SpectatedCarIndex { get; set; }
    public bool SliProNativeSupport { get; set; }
    public byte NumberOfMarshallZones { get; set; }
    public MarshallZone[] MarshallZones { get; set; }
    public SafetyCarStatus CurrentSafetyCarStatus { get; set; }
    public bool IsNetworkGame { get; set; }
    public byte NumberOfWeatherForecastSamples { get; set; }
    public WeatherForecastSample[] WeatherForecastSamples { get; set; }
    public bool ForecastIsAccurate { get; set; } // New in F1 2021
    public byte AiDifficulty { get; set; }
    public uint SeasonLinkIdentifier { get; set; }
    public uint WeekendLinkIdentifier { get; set; }
    public uint SessionLinkIdentifier { get; set; }
    public byte PitStopWindowIdealLap { get; set; }
    public byte PitStopWindowLatestLap { get; set; }
    public byte PitStopRejoinPosition { get; set; }
    public bool SteeringAssist { get; set; }
    public byte BreakingAssist { get; set; }
    public byte GearboxAssist { get; set; }
    public bool PitAssist { get; set; }
    public bool PitReleaseAssist { get; set; }
    public bool ErsAssist { get; set; }
    public bool DrsAssist { get; set; }
    public byte DynanicRacingLine { get; set; }
    public bool DynamicRacingLineType3D { get; set; }

    public GameMode GameMode { get; set; }
    public RuleSet RuleSet { get; set;}
    public uint TimeOfDay { get; set; }
    public SessionLength SessionLength { get; set; }
}