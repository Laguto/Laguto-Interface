﻿using Laguto.Lib.Enums;

namespace Laguto.Lib.Models
{
    public class Header
    {
        public const int HEADER_LENGTH = 24;
        public PackageFormat PackageFormat { get; set; }
        public byte GameMajorVersion { get; set; }
        public byte GameMinorVersion { get; set; }
        public byte PacketVersion { get; set; }
        public PacketType PacketType { get; set; }
        public ulong UniqueSessionId { get; set; }
        public float SessionTime { get; set; }
        public uint FrameIdentifier { get; set; }
        public byte PlayerCarIndex { get; set; }
        public byte SecondaryPlayerCarIndex { get; set; } // 255 if no second player.
    }
}
