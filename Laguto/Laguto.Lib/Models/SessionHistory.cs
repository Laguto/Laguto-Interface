﻿namespace Laguto.Lib.Models;

public class SessionHistory : AbstractPackage
{
    public byte CarIndex { get; set; }
    public byte NumberOfLaps { get; set; }
    public byte NumberOfTyreStints { get; set; }
    public byte BestLapTimeLapNumber { get; set; }
    public byte BestSector1LapNumber { get; set; }
    public byte BestSector2LapNumber { get; set; }
    public byte BestSector3LapNumber { get; set; }

    public LapHistory[] LapHistoryData { get; set; } // 100
    public TyreStintHistory[] TyreStintHistoryData { get; set; } // 8

    public class LapHistory
    {
        public uint LapTimeInMs { get; set; }
        public ushort Sector1TimeInMs { get; set; }
        public ushort Sector2TimeInMs { get; set; }
        public ushort Sector3TimeInMs { get; set; }
        public byte LapValidBitFlags { get; set; }
    }

    public class TyreStintHistory
    {
        public byte EndLap { get; set; }
        public byte TyreActualCompound { get; set; }
        public byte TyreVisualCompound { get; set; }
    }
}