﻿namespace Laguto.Lib.Models
{

    public class Motion : AbstractPackage
    {
        public CarMotionData[] FieldMotionData { get; set; }

        // Extra player car ONLY data
        public float[] SuspensionPosition { get; set; }
        public float[] SuspensionVelocity { get; set; }
        public float[] SuspensionAcceleration { get; set; }
        public float[] WheelSpeed { get; set; }
        public float[] WheelSlip { get; set; }
        public float LocalVelocityX { get; set; }
        public float LocalVelociytY { get; set; }
        public float LocalVelocityZ { get; set; }
        public float AngularVelocityX { get; set; }
        public float AngularVelocityY { get; set; }
        public float AngularVelocityZ { get; set; }
        public float AngularAccelerationX { get; set; }
        public float AngularAccelerationY { get; set; }
        public float AngularAccelerationZ { get; set; }
        public float FrontWheelsAngle { get; set; }

        public class CarMotionData
        {
            public float WorldPositionX { get; set; }
            public float WorldPositionY { get; set; }
            public float WorldPositionZ { get; set; }
            public float WorldVelocityX { get; set; }
            public float WorldVelocityY { get; set; }
            public float WorldVelocityZ { get; set; }

            public short WorldForwardDirX { get; set; }
            public short WorldForwardDirY { get; set; }
            public short WorldForwardDirZ { get; set; }

            public short WorldRightDirX { get; set; }
            public short WorldRightDirY { get; set; }
            public short WorldRightDirZ { get; set; }

            public float GForceLateral { get; set; }
            public float GForceLongitudinal { get; set; }
            public float GForceVertical { get; set; }
            public float Yaw { get; set; }
            public float Pitch { get; set; }
            public float Roll { get; set; }
        }
    }
}