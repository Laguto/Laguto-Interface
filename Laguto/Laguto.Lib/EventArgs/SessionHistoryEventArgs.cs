﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class SessionHistoryEventArgs  : System.EventArgs
{
    public SessionHistory SessionHistory { get; private set; }

    
    public SessionHistoryEventArgs(SessionHistory sessionHistory)
    {
        SessionHistory = sessionHistory;
    }

}