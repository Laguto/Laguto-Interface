﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class CarSetupPackageEventArgs : System.EventArgs
{
    public CarSetup CarSetup { get; private set; }

    public CarSetupPackageEventArgs(CarSetup carSetup)
    {
        CarSetup = carSetup;
    }
}