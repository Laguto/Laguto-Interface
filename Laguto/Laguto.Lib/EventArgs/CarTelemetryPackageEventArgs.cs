﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class CarTelemetryPackageEventArgs : System.EventArgs
{
    public CarTelemetry CarTelemetry { get; private set; }

    public CarTelemetryPackageEventArgs(CarTelemetry carTelemetry)
    {
        CarTelemetry = carTelemetry;
    }
}