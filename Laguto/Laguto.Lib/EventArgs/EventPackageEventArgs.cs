﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class EventPackageEventArgs : System.EventArgs
{
    public Event EventEntry { get; private set; }
    
    public EventPackageEventArgs(Event eventEntry)
    {
        EventEntry = eventEntry;
    }
}