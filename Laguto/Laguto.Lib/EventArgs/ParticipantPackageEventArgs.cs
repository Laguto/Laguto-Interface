﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class ParticipantPackageEventArgs : System.EventArgs
{
    public Participants Participants { get; private set; }

    public ParticipantPackageEventArgs(Participants participants)
    {
        Participants = participants;
    }
}