﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class LobbyPackageEventArgs : System.EventArgs
{
    public Lobby Lobby { get; private set; }

    public LobbyPackageEventArgs(Lobby lobby)
    {
        Lobby = lobby;
    }
}