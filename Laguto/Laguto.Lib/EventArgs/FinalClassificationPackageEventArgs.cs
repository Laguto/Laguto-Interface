﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class FinalClassificationPackageEventArgs : System.EventArgs
{
    public FinalClassification FinalClassification { get; private set; }
    
    public FinalClassificationPackageEventArgs(FinalClassification finalClassification)
    {
        FinalClassification = finalClassification;
    }
}