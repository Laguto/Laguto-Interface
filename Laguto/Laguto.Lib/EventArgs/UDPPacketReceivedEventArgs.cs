﻿namespace Laguto.Lib.EventArgs;

public class UdpPacketReceivedEventArgs : System.EventArgs
{
    public byte[] Buffer { get; private set; }
    
    public UdpPacketReceivedEventArgs(byte[] buffer)
    {
        Buffer = buffer;
    }
}