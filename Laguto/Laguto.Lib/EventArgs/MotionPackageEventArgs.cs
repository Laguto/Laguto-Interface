﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class MotionPackageEventArgs : System.EventArgs
{
    public Motion Motion { get; private set;}
    
    public MotionPackageEventArgs(Motion motion)
    {
        Motion = motion;
    }
}