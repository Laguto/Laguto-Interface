﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class SessionPackageEventArgs : System.EventArgs
{
    public Session Session { get; private set; }

    public SessionPackageEventArgs(Session session)
    {
        Session = session;
    }
}