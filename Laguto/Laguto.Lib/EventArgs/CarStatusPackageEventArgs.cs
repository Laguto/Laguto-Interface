﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class CarStatusPackageEventArgs : System.EventArgs
{
    public CarStatus CarStatus { get; private set; }
    
    public CarStatusPackageEventArgs(CarStatus carStatus)
    {
        CarStatus = carStatus;
    }
}