﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class CarDamagePackageEventArgs : System.EventArgs
{
    public CarDamage CarDamage { get; private set; }
    
    public CarDamagePackageEventArgs(CarDamage carStatus)
    {
        CarDamage = carStatus;
    }
}