﻿using Laguto.Lib.Models;

namespace Laguto.Lib.EventArgs;

public class LapPackageEventArgs : System.EventArgs
{
    public Lap Lap { get; private set; }
    
    public LapPackageEventArgs(Lap lap)
    {
        Lap = lap;
    }

    
}