using System.Text.RegularExpressions;
using Laguto.Interface.Controller;
using Laguto.Interface.Services;
using Laguto.Lib.Utility;
using Microsoft.OpenApi.Models;

/* Configuration flags */
bool openBrowser = true; // Open browser automatically at start

/* Check for custom arguments */
foreach(string arg in args)
{
    if(arg.StartsWith("--"))
    {
        string command = arg.Substring(2);
        if (command.Equals("no-browser"))
        {            
            openBrowser = false;
            Console.WriteLine("Starting without Browser");
        }
    }
}

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSingleton<ComponentService>();
builder.Services.AddControllers();

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo()
    {
        Version = "v1",
        Title = "Laguto API",
        Description = "API to request information delivered by the F1 22 game",
        Contact = new OpenApiContact()
        {
            Name = "DNF League Management",
            //Maybe needs to be changed according to the impressum url
            Url = new Uri("https://laguto.de")
        }
    });
});

var app = builder.Build();

app.MapWhen(ctx => ctx.Request.Path.StartsWithSegments("/api"), api =>
{
    api.UseRouting();
    api.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
});

app.MapWhen(ctx => !ctx.Request.Path.StartsWithSegments("/api"), api =>
{
    app.UseSpa(spa =>
    {
        spa.UseProxyToSpaDevelopmentServer("https://web.laguto.de");
    });
});

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        options.RoutePrefix = "swagger";
    });
}

app.UseRouting();
var assemblyVersion = typeof(Program).Assembly.GetName().Version;

Console.WriteLine("   __                   _        ");
Console.WriteLine("  / /  __ _  __ _ _   _| |_ ___  ");
Console.WriteLine(" / /  / _` |/ _` | | | | __/ _ \\ ");
Console.WriteLine("/ /__| (_| | (_| | |_| | || (_) |");
Console.WriteLine("\\____/\\__,_|\\__, |\\__,_|\\__\\___/ ");
Console.WriteLine("            |___/                ");
Console.WriteLine("####################################");
Console.WriteLine("Information: Laguto.de");
Console.WriteLine("====================================");
Console.WriteLine("Version: v" + assemblyVersion);
Console.WriteLine($"Laguto Address: {Util.GetLocalIPAddress()}:5000");
Console.WriteLine("====================================");

using (HttpClient client = new HttpClient(new HttpClientHandler() {AllowAutoRedirect = false}))
{
    var content = client.GetAsync("https://codeberg.org/laguto/Laguto-Interface/releases/latest").Result.Content;
    var contentAsString = await content.ReadAsStringAsync();

    var versionNumber = Regex.Match(contentAsString, "\\/v([0-9\\.]*)").Groups[1].Value;

    if (!versionNumber.Equals(assemblyVersion.ToString()))
    {
        PingController.NewVersionAvailable = true;
        Console.WriteLine($"New version v{versionNumber} is available for download");
        Console.WriteLine("https://codeberg.org/laguto/Laguto-Interface/releases/latest");
        Console.WriteLine("####################################");
    }
}

if(openBrowser)
    System.Diagnostics.Process.Start("cmd", "/C start http://" + Util.GetLocalIPAddress() + ":5000");


app.Run();