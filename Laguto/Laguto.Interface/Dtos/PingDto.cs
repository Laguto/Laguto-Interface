﻿namespace Laguto.Interface.Dtos;

public class PingDto
{
    public string Address { get; set; }
}