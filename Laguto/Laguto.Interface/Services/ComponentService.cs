﻿using Laguto.Lib.Components;
using Laguto.Lib.Handlers;
using Laguto.Lib.Listener;
using Laguto.Lib.Models;
using Laguto.Lib.Utility;

namespace Laguto.Interface.Services;

public class ComponentService
{
    public TableComponent TableComponent { get; private set; }
    public WeatherComponent WeatherComponent { get; private set;}
    public ResultComponent ResultComponent { get; private set;}
    
    public LapTimeComponent LapTimeComponent { get; private set; }
    public SessionComponent SessionComponent { get; private set; }

    public CarDamageComponent CarDamageComponent { get; private set; }
    public SessionLogComponent SessionLogComponent { get; private set; }
    
    public UtilityComponent UtilityComponent { get; set; }

    public LapPositionComponent LapPositionComponent { get; private set; }

    private PackageHandler _handler;

    private ulong _sessionId;
    public ComponentService()
    {
        _handler = new PackageHandler(new UDPListener(20777));
        

        UtilityComponent = new UtilityComponent();
        
        ResultComponent = new ResultComponent(UtilityComponent);
        LapPositionComponent = new LapPositionComponent(UtilityComponent);
        TableComponent = new TableComponent(UtilityComponent);
        WeatherComponent = new WeatherComponent(UtilityComponent);
        LapTimeComponent = new LapTimeComponent(UtilityComponent);
        CarDamageComponent = new CarDamageComponent(UtilityComponent);
        SessionLogComponent = new SessionLogComponent();
        SessionComponent = new SessionComponent();
        PrepareSubscriptions();
    }

    private void PrepareSubscriptions()
    {
        _handler.LapPackageReceivedEvent += (sender, args) =>
        {
            UtilityComponent.UpdateLap(args.Lap);
            TableComponent.UpdateLap(args.Lap);
            LapTimeComponent.UpdateLap(args.Lap);
            LapPositionComponent.UpdateLap(args.Lap);
        };

        _handler.ParticipantPackageReceivedEvent += (sender, args) =>
        {
            UtilityComponent.UpdateParticipants(args.Participants);
            TableComponent.UpdateParticipants(args.Participants);
            ResultComponent.UpdateParticipants(args.Participants);
            LapPositionComponent.UpdateParticipants(args.Participants);
        };
        
        _handler.SessionHistoryPackageReceivedEvent += (sender, args) =>
        {
            TableComponent.UpdateSessionHistory(args.SessionHistory);
            ResultComponent.UpdateSessionHistory(args.SessionHistory);
            LapTimeComponent.UpdateSessionHistory(args.SessionHistory);
        };
        
        _handler.CarStatusPackageReceivedEvent += (sender, args) =>
        {
            TableComponent.UpdateCarStatus(args.CarStatus);
        };
        
        _handler.CarTelemetryPackageReceivedEvent += (sender, args) =>
        {
        };
        
        _handler.FinalClassificationPackageReceivedEvent += (sender, args) =>
        {
            ResultComponent.UpdateFinalClassification(args.FinalClassification);
        };
        
        _handler.CarDamagePackageReceivedEvent += (sender, args) =>
        {
            CarDamageComponent.UpdateCarDamage(args.CarDamage);
        };

        _handler.SessionPackageReceivedEvent += (sender, args) =>
        {
            UtilityComponent.UpdateSession(args.Session);
            WeatherComponent.UpdateSession(args.Session);
            SessionComponent.UpdateSession(args.Session);
            LapPositionComponent.UpdateSession(args.Session);
            TableComponent.UpdateSession(args.Session);
            this.CheckForNewSession(args.Session);
        };

        _handler.EventPackageReceivedEvent += (sender, args) =>
        {
            SessionLogComponent.UpdateEvent(args.EventEntry);
        };
    }
    

    public TableComponent GetTableComponent()
    {
        return TableComponent;
    }

    public WeatherComponent GetWeatherComponent()
    {
        return WeatherComponent;
    }

    public ResultComponent GetResultComponent()
    {
        return ResultComponent;
    }

    public LapTimeComponent GetLapTimeComponent()
    {
        return LapTimeComponent;
    }

    public SessionComponent GetSessionComponent()
    {
        return SessionComponent;
    }

    public CarDamageComponent GetCarDamageComponent()
    {
        return CarDamageComponent;
    }

    public SessionLogComponent GetSessionLogComponent()
    {
        return SessionLogComponent;
    }

    public LapPositionComponent GetLapPositionComponent()
    {
        return LapPositionComponent;
    }

    private void CheckForNewSession(AbstractPackage package)
    {
        if(package.Header.UniqueSessionId != this._sessionId)
        {
            this._sessionId = package.Header.UniqueSessionId;
            this.ResetComponents();
        }
    }

    public void ResetComponents()
    {
        UtilityComponent = new UtilityComponent();
        ResultComponent = new ResultComponent(UtilityComponent);
        TableComponent = new TableComponent(UtilityComponent);
        WeatherComponent = new WeatherComponent(UtilityComponent);
        LapTimeComponent = new LapTimeComponent(UtilityComponent);
        CarDamageComponent = new CarDamageComponent(UtilityComponent);
        SessionLogComponent = new SessionLogComponent();
        SessionComponent = new SessionComponent();
        SessionLogComponent = new SessionLogComponent();
    }
}

