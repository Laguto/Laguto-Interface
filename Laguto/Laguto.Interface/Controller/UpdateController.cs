﻿using System.Diagnostics;
using Laguto.Interface.Services;
using Laguto.Lib.Components;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Laguto.Interface.Controller;

[ApiController]
[Route("api/[controller]")]
public class UpdateController : ControllerBase
{
    private ComponentService _componentService;

    public UpdateController(ComponentService componentService)
    {
        _componentService = componentService;
    }

    [HttpGet("fast")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult> GetUpdateFast(CancellationToken cancellationToken)
    {
        Response.Headers.Add("Content-Type", "text/event-stream");

        //Would be better to make this an event, but we know this only happens once or twice so its ok
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                ComponentStreamDTO dto = new ComponentStreamDTO();
                dto.type = "LapPosition";
                dto.data = _componentService.GetLapPositionComponent();
                var lapPositionJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{lapPositionJson}\n\n");
                await Response.Body.FlushAsync();

                dto = new ComponentStreamDTO();
                dto.type = "Table";
                dto.data = _componentService.GetTableComponent();
                var tableJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{tableJson}\n\n");
                await Response.Body.FlushAsync();

                await Task.Delay(500, cancellationToken);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        return new EmptyResult();
    }

    [HttpGet("medium")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult> GetUpdateMedium(CancellationToken cancellationToken)
    {
        Response.Headers.Add("Content-Type", "text/event-stream");

        //Would be better to make this an event, but we know this only happens once or twice so its ok
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                ComponentStreamDTO dto = new ComponentStreamDTO();
                dto.type = "Session";
                dto.data = _componentService.GetSessionComponent();
                var sessionJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{sessionJson}\n\n");
                await Response.Body.FlushAsync();

                dto = new ComponentStreamDTO();
                dto.type = "CarDamage";
                dto.data = _componentService.GetCarDamageComponent();
                var carDamageJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{carDamageJson}\n\n");
                await Response.Body.FlushAsync();

                dto = new ComponentStreamDTO();
                dto.type = "SessionLog";
                dto.data = _componentService.GetSessionLogComponent();
                var sessionLogJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{sessionLogJson}\n\n");
                await Response.Body.FlushAsync();

                BooleanStreamDTO dtoB = new BooleanStreamDTO();
                dtoB.type = "IsRaceFinished";
                dtoB.data = _componentService.GetResultComponent().IsRaceFinished;
                var isRaceFinishedJson = JsonConvert.SerializeObject(dtoB);
                await Response.WriteAsync($"data:{isRaceFinishedJson}\n\n");
                await Response.Body.FlushAsync();

                await Task.Delay(1000, cancellationToken);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
        return new EmptyResult();
    }
    
    [HttpGet("long")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult> GetUpdateLong(CancellationToken cancellationToken)
    {
        Response.Headers.Add("Content-Type", "text/event-stream");

        //Would be better to make this an event, but we know this only happens once or twice so its ok
        while (!cancellationToken.IsCancellationRequested)
        {
            try
            {
                ComponentStreamDTO dto = new ComponentStreamDTO();
                dto.type = "Weather";
                dto.data = _componentService.GetWeatherComponent();
                var weatherJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{weatherJson}\n\n");
                await Response.Body.FlushAsync();

                dto = new ComponentStreamDTO();
                dto.type = "LapTime";
                dto.data = _componentService.GetLapTimeComponent();
                var lapTimeJson = JsonConvert.SerializeObject(dto);
                await Response.WriteAsync($"data:{lapTimeJson}\n\n");
                await Response.Body.FlushAsync();
                
                await Task.Delay(5000, cancellationToken);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
        return new EmptyResult();
    }
}


public class AbstractStreamDTO
{
    public string type { get; set; }    
}

public class ComponentStreamDTO : AbstractStreamDTO
{
    public AbstractComponent data { get; set; }
}

public class BooleanStreamDTO : AbstractStreamDTO
{
    public bool data { get; set; }
}