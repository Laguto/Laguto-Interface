using Laguto.Interface.Dtos;
using Laguto.Interface.Services;
using Laguto.Lib.Utility;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Laguto.Interface.Controller;

[ApiController]
[Route("api/[controller]")]
public class PingController : ControllerBase
{

    private ComponentService _componentService;
    public static bool NewVersionAvailable = false;

    public PingController(ComponentService componentService)
    {
        _componentService = componentService;
    }

    /// <summary>
    /// Ping, to check if tool is available
    /// </summary>
    /// <returns></returns>
    /// <response code="200">Returns a Pingdto with the ipadress of the machine</response>
    [ProducesResponseType(typeof(PingDto), StatusCodes.Status200OK)]
    [HttpGet]
    public ActionResult GetPing()
    {        
        
        return Ok(NewVersionAvailable);
    }

}