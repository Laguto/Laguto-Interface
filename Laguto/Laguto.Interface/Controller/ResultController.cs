﻿using Laguto.Interface.Services;
using Laguto.Lib.Components;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Laguto.Interface.Controller;

[ApiController]
[Route("api/[controller]")]
public class ResultController : ControllerBase
{
    private ComponentService _componentService;

    public ResultController(ComponentService componentService)
    {
        _componentService = componentService;
    }
    
    /// <summary>
    /// Get informations about the session result
    /// </summary>
    /// <returns>a object with the session results</returns>
    /// <response code="200">Returns the information for the result</response>
    /// <response code="400">If the Resultcomponent is null</response>
    /// <response code="204">If the session hasn't finished yet</response>
    [HttpGet]
    [ProducesResponseType(typeof(ResultComponent), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public ActionResult GetResult()
    {        
        if (_componentService.GetResultComponent() == null)
            return BadRequest();

        if (!_componentService.GetResultComponent().IsRaceFinished)
        {
            return NoContent();
        }
        
        return Ok(JsonConvert.SerializeObject(_componentService.GetResultComponent()));
    }

    /// <summary>
    /// Resets all informations of the ended session
    /// </summary>
    /// <returns></returns>
    /// <response code="200"></response>
    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Produces("application/json")]
    public ActionResult DeleteResults()
    {
        _componentService.ResetComponents();
        return Ok();
    }
}